# Estrutura básica do trabalho de CSB30 - Introdução A Bancos De Dados

Equipe VMA

- O que vocês vão implementar
Sistema de gerencia de mesas, pedidos, garçons para um restaurante.
- Quais tecnologias vão utilizar
Java + SQL
- Por que decidiram pelo tema
Alta possibilidade de aplicação do conhecimento adquirido em diversos comércios.
- Quais os desafios relacionados com bancos de dados (consultas, modelos, etc.)
A relação entre pedidos e o cardápio tivemos um pouco de dificuldade para decidir qual a melhor modelagem para possibilitar a inserção de pratos para um só pedido.
- Quais os desafios relacionados com suas áreas de interesse
Possibilidade de expansão do sistema para aplicação em comércios reis de grande porte, facilitando a gerencia do negócio.
- Quais conhecimentos novos vocês pretendem adquirir
Regras negociais aplicáveis a comércios em geral, conhecimento técnico para o desenvolvimento do projeto.
- O que vocês já produziram (protótipos, esquemas de telas, teste de codificação de partes críticas, etc.)
Modelagem v1.0: 
 
Para cada integrante do grupo, preencha os campos abaixo:

- Nome: Vickie da Costa Aranha
- Qual o seu foco na implementação do trabalho?
Apoio no desenvolvimento da aplicação e do banco.
- Já teve alguma experiência com as tecnologias que serão usadas no trabalho? Quais?
SQL já possuo experiência, Java somente o básico visto em aula de programação 2.
- Qual aspecto do trabalho te interessa mais?
A possibilidade de agregação do conhecimento para regras negociais aplicáveis a comércios em geral.

- Nome: André Otavio Pedrofeza de Oliveira
- Qual o seu foco na implementação do trabalho?
Desenvolvimento da interface em Java e integração com o banco.
- Já teve alguma experiência com as tecnologias que serão usadas no trabalho? Quais?
Sim, com a linguagem Java.
- Qual aspecto do trabalho te interessa mais?
Desenvolver um sistema que seja útil e possa ser aplicado em um comércio real.

- Nome Miguel Ferreira Chagas Neto
- Qual o seu foco na implementação do trabalho?
-Praticidade em um projeto voltado para o mercado
- Já teve alguma experiência com as tecnologias que serão usadas no trabalho? Quais?
- Não tenho experiência com nenhuma das tecnologias
- Qual aspecto do trabalho te interessa mais?
-O aspecto que mais me interessa é a abrangência do uso desta tecnologia atualmente



## Integrantes do grupo



- Vickie da Costa Aranha, 1660101, @vickie.costa 
- Miguel ferreira chagas neto , 1796453 , @migueln
- André Otavio Pedrofeza de Oliveira  ,1830163 ,@andrepdo

## Descrição da aplicação a ser desenvolvida 

Descreva aqui uma visão geral da aplicação que será desenvolvida pelo grupo durante o semestre. **Este texto deverá ser escrito quando requisitado pelo professor.** O conteúdo vai evoluir à medida em que o grupo avança com a implementação.
