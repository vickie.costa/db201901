/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package redebd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

/**
 *
 * @author mchag
 */
public class RedeBd {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        String url = "http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/person.xml";
        String xml = crunchifyGetURLContents(url);
        String url2 = "http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/music.xml";
        String xml2 = crunchifyGetURLContents(url2);
        String url3 = "http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/movie.xml";
        String xml3 = crunchifyGetURLContents(url3);
        String url4 = "http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/knows.xml";
        String xml4 = crunchifyGetURLContents(url4);

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document person = builder.parse(new InputSource(new StringReader(xml)));

            DocumentBuilderFactory factory2 = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder2 = factory2.newDocumentBuilder();
            Document music = builder2.parse(new InputSource(new StringReader(xml2)));

            DocumentBuilderFactory factory3 = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder3 = factory3.newDocumentBuilder();
            Document movies = builder3.parse(new InputSource(new StringReader(xml3)));

            DocumentBuilderFactory factory4 = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder4 = factory4.newDocumentBuilder();
            Document knows = builder4.parse(new InputSource(new StringReader(xml4)));

            person.getDocumentElement().normalize();
            music.getDocumentElement().normalize();
            movies.getDocumentElement().normalize();
            knows.getDocumentElement().normalize();

            NodeList nListPerson = person.getElementsByTagName("Person");
            NodeList nListMusic = music.getElementsByTagName("LikesMusic");
            NodeList nListMovie = movies.getElementsByTagName("LikesMovie");
            NodeList nListKnows = knows.getElementsByTagName("Knows");

            Connection c = null;
            Statement stmt = null;
            //Cria as tabelas
            try {
                Class.forName("org.postgresql.Driver");
                c = DriverManager.getConnection("jdbc:postgresql://200.134.10.32:5432/1901VMA",
                        "1901VMA", "786333");
                System.out.println("Opened database successfully");
                stmt = c.createStatement();
                String sql = "CREATE TABLE Usuario (\n"
                        + " login VARCHAR(100) NOT NULL,\n"
                        + " nome VARCHAR(100) NOT NULL,\n"
                        + " cidade VARCHAR(100) NOT NULL,\n"
                        + " birthday VARCHAR(100) NOT NULL,\n"
                        + " PRIMARY KEY(login)\n"
                        + ");\n"
                        + "\n"
                        + "CREATE TABLE Conhece (\n"
                        + " Usua1 VARCHAR(100) NOT NULL,\n"
                        + " Usua2 VARCHAR(100) NOT NULL,\n"
                        + " FOREIGN KEY(Usua1)\n"
                        + " REFERENCES Usuario(login)\n"
                        + " ON DELETE NO ACTION\n"
                        + " ON UPDATE NO ACTION,\n"
                        + " FOREIGN KEY(Usua2)\n"
                        + " REFERENCES Usuario(login)\n"
                        + " ON DELETE NO ACTION\n"
                        + " ON UPDATE NO ACTION\n"
                        + ");\n"
                        + "\n"
                        + "\n"
                        + "CREATE TABLE Bloqueio (\n"
                        + " Usuario VARCHAR(100) NOT NULL,\n"
                        + " Razao_Bloqueio VARCHAR(100) NOT NULL,\n"
                        + " Usuario_block VARCHAR(100) NOT NULL,\n"
                        + " FOREIGN KEY(Usuario)\n"
                        + " REFERENCES Usuario(login)\n"
                        + " ON DELETE NO ACTION\n"
                        + " ON UPDATE NO ACTION, \n"
                        + " FOREIGN KEY(Usuario_block)\n"
                        + " REFERENCES Usuario(login)\n"
                        + " ON DELETE NO ACTION\n"
                        + " ON UPDATE NO ACTION \n"
                        + ");\n"
                        + "\n"
                        + "\n"
                        + "CREATE TABLE Artistas (\n"
                        + " pais VARCHAR(100) NOT NULL,\n"
                        + " nomeArtistico VARCHAR(100) NOT NULL,\n"
                        + " generoMusical VARCHAR(100) NOT NULL,\n"
                        + " PRIMARY KEY(nomeArtistico)\n"
                        + ");\n"
                        + "\n"
                        + "\n"
                        + "CREATE TABLE Curte (\n"
                        + " nota INTEGER NOT NULL,\n"
                        + " Usuario VARCHAR(100) NOT NULL,\n"
                        + " Artistas VARCHAR(100) NOT NULL,\n"
                        + " FOREIGN KEY(Usuario)\n"
                        + " REFERENCES Usuario(login)\n"
                        + " ON DELETE NO ACTION\n"
                        + " ON UPDATE NO ACTION,\n"
                        + " FOREIGN KEY(Artistas)\n"
                        + " REFERENCES Artistas(nomeArtistico)\n"
                        + " ON DELETE NO ACTION\n"
                        + " ON UPDATE NO ACTION\n"
                        + ");\n"
                        + "\n"
                        + "CREATE TABLE Musico (\n"
                        + " nomeReal VARCHAR(100) NOT NULL,\n"
                        + " estiloMusical VARCHAR(100) NOT NULL,\n"
                        + " dataNascimento DATE NOT NULL,\n"
                        + " PRIMARY KEY(nomeReal)\n"
                        + ");\n"
                        + "\n"
                        + "CREATE TABLE Cantores (\n"
                        + " nomeArtistico VARCHAR(100) NOT NULL,\n"
                        + " nomeReal VARCHAR(100) NOT NULL,\n"
                        + " FOREIGN KEY(nomeArtistico)\n"
                        + " REFERENCES Artistas(nomeArtistico)\n"
                        + " ON DELETE NO ACTION\n"
                        + " ON UPDATE NO ACTION,\n"
                        + " FOREIGN KEY(nomeReal)\n"
                        + " REFERENCES Musico(nomeReal)\n"
                        + " ON DELETE NO ACTION\n"
                        + " ON UPDATE NO ACTION\n"
                        + ");\n"
                        + "\n"
                        + "\n"
                        + "\n"
                        + "CREATE TABLE Duplas_possui (\n"
                        + " nomeArtistico VARCHAR(100) NOT NULL,\n"
                        + " nomeReal VARCHAR(100) NOT NULL,\n"
                        + " FOREIGN KEY(nomeArtistico)\n"
                        + " REFERENCES Artistas(nomeArtistico)\n"
                        + " ON DELETE NO ACTION\n"
                        + " ON UPDATE NO ACTION,\n"
                        + " FOREIGN KEY(nomeReal)\n"
                        + " REFERENCES Musico(nomeReal)\n"
                        + " ON DELETE NO ACTION\n"
                        + " ON UPDATE NO ACTION\n"
                        + ");\n"
                        + "\n"
                        + "\n"
                        + "\n"
                        + "CREATE TABLE Grupos_possui (\n"
                        + " nomeArtistico VARCHAR(100) NOT NULL,\n"
                        + " nomeReal VARCHAR(100) NOT NULL,\n"
                        + " FOREIGN KEY(nomeArtistico)\n"
                        + " REFERENCES Artistas(nomeArtistico)\n"
                        + " ON DELETE NO ACTION\n"
                        + " ON UPDATE NO ACTION,\n"
                        + " FOREIGN KEY(nomeReal)\n"
                        + " REFERENCES Musico(nomeReal)\n"
                        + " ON DELETE NO ACTION\n"
                        + " ON UPDATE NO ACTION\n"
                        + ");\n"
                        + "\n"
                        + "CREATE TABLE Filme (\n"
                        + " nome VARCHAR(100) NOT NULL,\n"
                        + " dataLancamento DATE NOT NULL,\n"
                        + " PRIMARY KEY(nome)\n"
                        + ");\n"
                        + "\n"
                        + "CREATE TABLE Curte2(\n"
                        + " nota INTEGER NOT NULL,\n"
                        + " Usuario VARCHAR(100) NOT NULL,\n"
                        + " Filme VARCHAR(100) NOT NULL,\n"
                        + " FOREIGN KEY(Usuario)\n"
                        + " REFERENCES Usuario(login)\n"
                        + " ON DELETE NO ACTION\n"
                        + " ON UPDATE NO ACTION,\n"
                        + " FOREIGN KEY(Filme)\n"
                        + " REFERENCES Filme(nome)\n"
                        + " ON DELETE NO ACTION\n"
                        + " ON UPDATE NO ACTION\n"
                        + ");\n"
                        + "\n"
                        + "CREATE TABLE Categoria (\n"
                        + " desc_categoria VARCHAR(1000) NOT NULL,\n"
                        + " PRIMARY KEY(desc_categoria)\n"
                        + ");\n"
                        + "\n"
                        + "CREATE TABLE Classifica (\n"
                        + " nome VARCHAR(100) NOT NULL,\n"
                        + " categoria VARCHAR(100) NOT NULL,\n"
                        + " FOREIGN KEY(nome)\n"
                        + " REFERENCES Filme(nome)\n"
                        + " ON DELETE NO ACTION\n"
                        + " ON UPDATE NO ACTION,\n"
                        + " FOREIGN KEY(categoria)\n"
                        + " REFERENCES Categoria(desc_categoria)\n"
                        + " ON DELETE NO ACTION\n"
                        + " ON UPDATE NO ACTION\n"
                        + ");\n"
                        + "\n"
                        + "\n"
                        + "\n"
                        + "CREATE TABLE Subcategoria (\n"
                        + " Categ1 VARCHAR(100) NOT NULL,\n"
                        + " Categ2 VARCHAR(100) NOT NULL,\n"
                        + " FOREIGN KEY(Categ1)\n"
                        + " REFERENCES Categoria(desc_categoria)\n"
                        + " ON DELETE NO ACTION\n"
                        + " ON UPDATE NO ACTION,\n"
                        + " FOREIGN KEY(Categ2)\n"
                        + " REFERENCES Categoria(desc_categoria)\n"
                        + " ON DELETE NO ACTION\n"
                        + " ON UPDATE NO ACTION\n"
                        + ");\n"
                        + "\n"
                        + "CREATE TABLE Diretor (\n"
                        + " id VARCHAR(100) NOT NULL,\n"
                        + " telefone VARCHAR(100) NOT NULL,\n"
                        + " endereco VARCHAR(100) NOT NULL,\n"
                        + " PRIMARY KEY(id)\n"
                        + ");\n"
                        + "\n"
                        + "CREATE TABLE Dirige (\n"
                        + " salarioAssociado VARCHAR(100) NOT NULL,\n"
                        + " id VARCHAR(100) NOT NULL,\n"
                        + " nomeFilme VARCHAR(100) NOT NULL,\n"
                        + " FOREIGN KEY(id)\n"
                        + " REFERENCES Diretor(id)\n"
                        + " ON DELETE NO ACTION\n"
                        + " ON UPDATE NO ACTION,\n"
                        + " FOREIGN KEY(nomeFilme)\n"
                        + " REFERENCES Filme(nome)\n"
                        + " ON DELETE NO ACTION\n"
                        + " ON UPDATE NO ACTION\n"
                        + ");\n"
                        + "\n"
                        + "CREATE TABLE Ator (\n"
                        + " id VARCHAR(100) NOT NULL,\n"
                        + " telefone VARCHAR(100) NOT NULL,\n"
                        + " endereco VARCHAR(100) NOT NULL,\n"
                        + " PRIMARY KEY(id)\n"
                        + ");\n"
                        + "\n"
                        + "CREATE TABLE Possui2 (\n"
                        + " id VARCHAR(100) NOT NULL,\n"
                        + " nomeFilme VARCHAR(100) NOT NULL,\n"
                        + " salarioAssociado VARCHAR(100) NOT NULL,\n"
                        + " FOREIGN KEY(id)\n"
                        + " REFERENCES Ator(id)\n"
                        + " ON DELETE NO ACTION\n"
                        + " ON UPDATE NO ACTION,\n"
                        + " FOREIGN KEY(nomeFilme)\n"
                        + " REFERENCES Filme(nome)\n"
                        + " ON DELETE NO ACTION\n"
                        + " ON UPDATE NO ACTION\n"
                        + ");";
                stmt.executeUpdate(sql);
                stmt.close();
                c.close();
            } catch (Exception e) {
                System.err.println(e.getClass().getName() + ": " + e.getMessage());

            }
            System.out.println("Table created successfully");

            //Insere nas tabela usuario
            try {
                Class.forName("org.postgresql.Driver");
                c = DriverManager.getConnection("jdbc:postgresql://200.134.10.32:5432/1901VMA",
                        "1901VMA", "786333");
                System.out.println("Opened database successfully");
                stmt = c.createStatement();
                //insere usuarios

                String sqlusuarios;

                for (int temp = 0; temp < nListPerson.getLength(); temp++) {
                    Node nNodePerson = nListPerson.item(temp);

                    if (nNodePerson.getNodeType() == Node.ELEMENT_NODE) {
                        try {
                            Element eElementPerson = (Element) nNodePerson;
                            sqlusuarios = "INSERT INTO Usuario VALUES('" + eElementPerson.getAttribute("uri")
                                    + "','" + eElementPerson.getAttribute("name") + "','"
                                    + eElementPerson.getAttribute("hometown") + "','"
                                    + eElementPerson.getAttribute("birthdate") + "');\n \n \n";

                            stmt.executeUpdate(sqlusuarios);
                        } catch (Exception e) {
                            System.err.println(e.getClass().getName() + ": " + e.getMessage());
                        }

                    }
                }
                //insere Conhecidos

                String sqlknows;
                sqlknows = "DELETE FROM conhece";
                stmt.executeUpdate(sqlknows);
                for (int temp = 0; temp < nListKnows.getLength(); temp++) {
                    Node nNodeKnows = nListKnows.item(temp);

                    if (nNodeKnows.getNodeType() == Node.ELEMENT_NODE) {
                        try {
                            Element eElementKnows = (Element) nNodeKnows;
                            sqlknows = "INSERT INTO conhece VALUES('" + eElementKnows.getAttribute("person")
                                    + "','" + eElementKnows.getAttribute("colleague") + "');\n \n \n";

                            stmt.executeUpdate(sqlknows);
                        } catch (Exception e) {
                            System.err.println(e.getClass().getName() + ": " + e.getMessage());
                        }

                    }
                }
//insere artistas

                String sqlArtista;
                for (int temp = 0; temp < nListMusic.getLength(); temp++) {
                    Node nNodeArtista = nListMusic.item(temp);

                    if (nNodeArtista.getNodeType() == Node.ELEMENT_NODE) {
                        try {
                            Element eElementArtista = (Element) nNodeArtista;
                            sqlArtista = "INSERT INTO artistas VALUES('PANGEIA','" + eElementArtista.getAttribute("bandUri")
                                    + "','Surpresa');\n \n \n";

                            stmt.executeUpdate(sqlArtista);
                        } catch (Exception e) {
                            System.err.println(e.getClass().getName() + ": " + e.getMessage());
                        }

                    }
                }

                //insere os filmes
                String sqlMovie;
                for (int temp = 0; temp < nListMovie.getLength(); temp++) {
                    Node nNodeMovie = nListMovie.item(temp);

                    if (nNodeMovie.getNodeType() == Node.ELEMENT_NODE) {
                        try {
                            Element eElementMovie = (Element) nNodeMovie;
                            sqlMovie = "INSERT INTO filme VALUES('" + eElementMovie.getAttribute("movieUri")
                                    + "','15-03-1892');\n \n \n";

                            stmt.executeUpdate(sqlMovie);
                        } catch (Exception e) {
                            System.err.println(e.getClass().getName() + ": " + e.getMessage());
                        }

                    }
                }

                // Insere gosto musical
                String sqlLikeMusic;
                sqlLikeMusic = "DELETE FROM curte";
                stmt.executeUpdate(sqlLikeMusic);
                for (int temp = 0; temp < nListMusic.getLength(); temp++) {
                    Node nNodeLikeMusic = nListMusic.item(temp);

                    if (nNodeLikeMusic.getNodeType() == Node.ELEMENT_NODE) {
                        try {
                            Element eElementLikeMusic = (Element) nNodeLikeMusic;
                            sqlLikeMusic = "INSERT INTO curte VALUES('" + eElementLikeMusic.getAttribute("rating")
                                    + "','" + eElementLikeMusic.getAttribute("person")
                                    + "','" + eElementLikeMusic.getAttribute("bandUri") + "' );\n \n \n";

                            stmt.executeUpdate(sqlLikeMusic);
                        } catch (Exception e) {
                            System.err.println(e.getClass().getName() + ": " + e.getMessage());
                        }

                    }
                }

                //Insere o gosta de filmes     
                String sqlLikeMovie;
                sqlLikeMovie = "DELETE FROM curte2";
                stmt.executeUpdate(sqlLikeMovie);
                for (int temp = 0; temp < nListMovie.getLength(); temp++) {
                    Node nNodeLikeMovie = nListMovie.item(temp);

                    if (nNodeLikeMovie.getNodeType() == Node.ELEMENT_NODE) {
                        try {
                            Element eElementLikeMovie = (Element) nNodeLikeMovie;
                            sqlLikeMovie = "INSERT INTO curte2 VALUES('" + eElementLikeMovie.getAttribute("rating")
                                    + "','" + eElementLikeMovie.getAttribute("person")
                                    + "','" + eElementLikeMovie.getAttribute("movieUri") + "' );\n \n \n";

                            stmt.executeUpdate(sqlLikeMovie);
                        } catch (Exception e) {
                            System.err.println(e.getClass().getName() + ": " + e.getMessage());
                        }

                    }
                }
                stmt.close();
                c.close();
            } catch (Exception e) {
                System.err.println(e.getClass().getName() + ": " + e.getMessage());
                System.exit(0);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String crunchifyGetURLContents(String myURL) {
        System.out.println("crunchifyGetURLContents() is hitting : " + myURL);
        StringBuilder sb = new StringBuilder();
        URLConnection urlConn = null;
        InputStreamReader in = null;
        try {
            URL url = new URL(myURL);
            urlConn = url.openConnection();
            if (urlConn != null) {
                urlConn.setReadTimeout(60 * 1000);
            }
            if (urlConn != null && urlConn.getInputStream() != null) {
                in = new InputStreamReader(urlConn.getInputStream(), Charset.defaultCharset());
                BufferedReader bufferedReader = new BufferedReader(in);
                if (bufferedReader != null) {
                    int cp;
                    while ((cp = bufferedReader.read()) != -1) {
                        sb.append((char) cp);
                    }
                    bufferedReader.close();
                }
            }
            in.close();
        } catch (Exception e) {
            throw new RuntimeException("Exception while calling URL:" + myURL, e);
        }

        return sb.toString();
    }
}
