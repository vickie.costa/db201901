/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package extract;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.json.JSONObject;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

/**
 *
 * @author mchag
 */
public class Extract_filme {
    String nome_frmt;
    String movie_uri;
    String diretor;
    String released;
    ArrayList<String> genres; 
    String rate;
    
    public Extract_filme(String id_filme){
        genres = new ArrayList<String>(); 
        String movie_id;
        movie_id=id_filme.replace("http://www.imdb.com/title/","");
        movie_id=movie_id.replace("/", "");
        movie_id=movie_id.replace(" ", "_");        
        String uri = "http://www.omdbapi.com/?i="+movie_id+"&r=xml&apikey=547af14c";
        movie_uri=id_filme;
        String xml = Extract_artista.crunchifyGetURLContents(uri);
        
                try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document movie = builder.parse(new InputSource(new StringReader(xml)));
            movie.getDocumentElement().normalize();
                
            	NodeList nList = movie.getElementsByTagName("movie");
	
		Node nNode = nList.item(0);


		if (nNode.getNodeType() == Node.ELEMENT_NODE) {

			Element eElement = (Element) nNode;
                        nome_frmt=eElement.getAttribute("title");
                        diretor=eElement.getAttribute("director");
                        genres=new ArrayList<String>(Arrays.asList(eElement.getAttribute("genre").split(", ")));;
                        released=eElement.getAttribute("released");
                
                        try{
                            String aux_nome=nome_frmt.replace(" ","+");
                            URL tmdbapi = new URL("https://api.themoviedb.org/3/search/movie?api_key=afaa1d3006b9192c989a5d29a51d1743&query="+aux_nome);
                            BufferedReader in = new BufferedReader(new InputStreamReader(tmdbapi.openStream()));
                            String tmdb = in.readLine();
                            JSONObject tmdb_json = new JSONObject(tmdb);
                            Float media=tmdb_json.getJSONArray("results").getJSONObject(0).getFloat("vote_average");
                            rate= String.valueOf(media);
                        }catch (Exception ex) {
                        rate=null;
                    }
		
                    }
                }catch (Exception ex) {
                     nome_frmt=null;
                     movie_uri=id_filme;
                     diretor=null;
                     released=null;
                     genres=null; 
                     rate=null;
                }
            

    }

    
}
