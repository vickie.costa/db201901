/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package extract;


import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
/**
 *
 * @author mchag
 */


public class Extract {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {

        Connection c = null;
        Statement stmt = null;
        ArrayList<String> id_filmes = new ArrayList<String>();
        ArrayList<String> id_bandas = new ArrayList<String>();
        ArrayList<Extract_filme> filmes = new ArrayList<Extract_filme>();
        ArrayList<Extract_artista> bandas = new ArrayList<Extract_artista>();
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql://200.134.10.32:5432/1901VMA", "1901VMA", "786333");
            c.setAutoCommit(false);

            stmt = c.createStatement();
            ResultSet rs_filme = stmt.executeQuery("SELECT * FROM filme;");

            while (rs_filme.next()) {
                id_filmes.add(rs_filme.getString("nome"));
            }
          
            ResultSet rs_banda = stmt.executeQuery("SELECT * FROM artistas;");
            while (rs_banda.next()) {
                id_bandas.add(rs_banda.getString("nomeartistico"));
            }

            rs_banda.close();
            rs_filme.close();
            stmt.close();
            c.close();
        } catch (Exception e) {

        }
       
        Extract_filme aux_filme;
        for (int i = 0; i <id_filmes.size(); i++) {
            aux_filme = new Extract_filme(id_filmes.get(i));
            filmes.add(aux_filme);
            System.out.println(filmes.get(i).nome_frmt);
        }

         Extract_artista aux_artista;
         for(int i=0; i<id_bandas.size();i++)
         {
         aux_artista=new Extract_artista(id_bandas.get(i));
         bandas.add(aux_artista);
         System.out.println(bandas.get(i).nome_frmt);
         }
        //bandas e filmes são os arrayslist com os filmes e bandas
    
       //movies.xml  
        try{
      DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();

        DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();

        Document document = documentBuilder.newDocument();

        // root element
        Element root = document.createElement("Movies");
        document.appendChild(root);

        for (int j = 0; j < filmes.size(); j++) {
                //Transformando genero em uma so string

            // employee element
            Element mv = document.createElement("Movie");

            root.appendChild(mv);

            Attr attr = document.createAttribute("id");
            attr.setValue("" + j);
            mv.setAttributeNode(attr);
            // set an attribute to staff element

            if (filmes.get(j).movie_uri == null) {
                Element mv_uri = document.createElement("uri");
                mv_uri.appendChild(document.createTextNode(""));
                mv.appendChild(mv_uri);
            } else {
                Element mv_uri = document.createElement("uri");
                mv_uri.appendChild(document.createTextNode(filmes.get(j).movie_uri));
                mv.appendChild(mv_uri);
            }

            if (filmes.get(j).nome_frmt == null) {
                Element mv_name = document.createElement("name");
                mv_name.appendChild(document.createTextNode(""));
                mv.appendChild(mv_name);
            } else {
                Element mv_name = document.createElement("name");
                mv_name.appendChild(document.createTextNode(filmes.get(j).nome_frmt));
                mv.appendChild(mv_name);
            }

            if (filmes.get(j).diretor == null) {
                Element mv_name = document.createElement("director");
                mv_name.appendChild(document.createTextNode(""));
                mv.appendChild(mv_name);
            } else {
                Element mv_director = document.createElement("director");
                mv_director.appendChild(document.createTextNode(filmes.get(j).diretor));
                mv.appendChild(mv_director);
            }

            if (filmes.get(j).genres != null) {
                for (int k = 0; k < filmes.get(j).genres.size(); k++) {
                    Element mv_genres = document.createElement("genres");
                    mv_genres.appendChild(document.createTextNode(filmes.get(j).genres.get(k)));
                    mv.appendChild(mv_genres);
                }
            } else {
                Element mv_genres = document.createElement("genres");
                mv_genres.appendChild(document.createTextNode(""));
                mv.appendChild(mv_genres);
            }
            if (filmes.get(j).rate == null) {
                Element mv_rate = document.createElement("rate");
                mv_rate.appendChild(document.createTextNode(""));
                mv.appendChild(mv_rate);
            } else {
                Element mv_rate = document.createElement("rate");
                mv_rate.appendChild(document.createTextNode(filmes.get(j).rate));
                mv.appendChild(mv_rate);
            }

            if (filmes.get(j).released == null) {
                Element mv_released = document.createElement("released");
                mv_released.appendChild(document.createTextNode(""));
                mv.appendChild(mv_released);
            } else {
                Element mv_released = document.createElement("released");
                mv_released.appendChild(document.createTextNode(filmes.get(j).released));
                mv.appendChild(mv_released);
            }

        }

            // create the xml file
        //transform the DOM Object to an XML File
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource domSource = new DOMSource(document);
        StreamResult streamResult = new StreamResult(new File("movies.xml"));
            // If you use
        // StreamResult result = new StreamResult(System.out);
        // the output will be pushed to the standard output ...
        // You can use that for debugging 

        transformer.transform(domSource, streamResult);

        System.out.println("Done creating XML File");

        }
        catch (Exception ex) {
            System.out.println("no costruiu");
        }
         
         //music.xml
        try{
        DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();

        DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();

        Document document = documentBuilder.newDocument();

        // root element
        Element root = document.createElement("Musics");
        document.appendChild(root);

        for (int j = 0; j < bandas.size(); j++) {
                //Transformando genero em uma so string

            // employee element
            Element bd = document.createElement("banda");

            root.appendChild(bd);

            Attr attr = document.createAttribute("id");
            attr.setValue("" + j);
            bd.setAttributeNode(attr);
            // set an attribute to staff element


            if (bandas.get(j).band_uri!=null) {
                Element bd_uri = document.createElement("uri");
                bd_uri.appendChild(document.createTextNode(bandas.get(j).band_uri));
                bd.appendChild(bd_uri);
            } else {
                Element bd_uri = document.createElement("uri");
                bd_uri.appendChild(document.createTextNode(""));
                bd.appendChild(bd_uri);
            }



            if (bandas.get(j).nome_frmt!=null) {
                Element bd_name = document.createElement("name");
                bd_name.appendChild(document.createTextNode(bandas.get(j).nome_frmt));
                bd.appendChild(bd_name);
            } else {
                Element bd_name = document.createElement("name");
                bd_name.appendChild(document.createTextNode(""));
                bd.appendChild(bd_name);
            }
           

 
            if (bandas.get(j).members!=null) {
                for (int k = 0; k < bandas.get(j).members.size(); k++) {
                    Element bd_genres = document.createElement("members");
                    bd_genres.appendChild(document.createTextNode(bandas.get(j).members.get(k)));
                    bd.appendChild(bd_genres);
                }
            } else {
                    Element bd_genres = document.createElement("members");
                    bd_genres.appendChild(document.createTextNode(""));
                    bd.appendChild(bd_genres);
            }
            

            if (bandas.get(j).genres!=null) {
                for (int k = 0; k < bandas.get(j).genres.size(); k++) {
                    Element bd_genres = document.createElement("genres");
                    bd_genres.appendChild(document.createTextNode(bandas.get(j).genres.get(k)));
                    bd.appendChild(bd_genres);
                }
            } else {
                    Element bd_genres = document.createElement("genres");
                    bd_genres.appendChild(document.createTextNode(""));
                    bd.appendChild(bd_genres);
            }
            
            if (bandas.get(j).rank!=null) {
                Element bd_rank = document.createElement("rank");
                bd_rank.appendChild(document.createTextNode(bandas.get(j).rank));
                bd.appendChild(bd_rank);
            } else {
                Element bd_rank = document.createElement("rank");
                bd_rank.appendChild(document.createTextNode(""));
                bd.appendChild(bd_rank);
            }


            if (bandas.get(j).city!=null) {
                Element bd_city = document.createElement("city");
                bd_city.appendChild(document.createTextNode(bandas.get(j).city));
                bd.appendChild(bd_city);
            } else {
                Element bd_city = document.createElement("city");
                bd_city.appendChild(document.createTextNode(""));
                bd.appendChild(bd_city);
            }

            if (bandas.get(j).country!=null) {
                Element bd_country = document.createElement("country");
                bd_country.appendChild(document.createTextNode(bandas.get(j).country));
                bd.appendChild(bd_country);
            } else {
                Element bd_country = document.createElement("country");
                bd_country.appendChild(document.createTextNode(""));
                bd.appendChild(bd_country);
            }


            }

        

            // create the xml file
        //transform the DOM Object to an XML File
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource domSource = new DOMSource(document);
        StreamResult streamResult = new StreamResult(new File("music.xml"));

                System.out.println("passoaqui");

        transformer.transform(domSource, streamResult);

        System.out.println("Done creating XML File");
           
        }
        catch (Exception ex) {
            System.out.println("no costruiu");
        }
    }

}
