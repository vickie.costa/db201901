/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package extract;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.json.JSONObject;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
/**
 *
 * @author mchag
 */
public class Extract_artista {

    String nome_frmt;
    String band_uri;
    ArrayList<String> members;
    ArrayList<String> genres;
    String city;
    String country;
    String rank;

    public Extract_artista(String nome_banda) {
        members = new ArrayList<String>();
        genres = new ArrayList<String>();

        band_uri = nome_banda;
        
        nome_banda = nome_banda.replace("https://en.wikipedia.org/wiki/", "");
        nome_frmt = nome_banda.replace("_", " ");
        nome_banda = nome_banda.replace(" ", "_");
        String uri = "http://dbpedia.org/data/" + nome_banda + ".rdf";

        String xml = Extract_artista.crunchifyGetURLContents(uri);

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document band = builder.parse(new InputSource(new StringReader(xml)));

            band.getDocumentElement().normalize();
             NodeList nList = null;
            String hometown = null;
            // MEMBROS DA BANDA
            try{
            nList = band.getElementsByTagName("dbo:bandMember");

            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;
                    String a = eElement.getAttribute("rdf:resource");
                    a = a.replace("http://dbpedia.org/resource/", "");
                    a = a.replace("_", " ");
                    members.add(a);
                }
            }
            }
            catch (Exception ex) {
               members=null;
                
            }
            // GENEROS DA BANDA

            try {
                nList = band.getElementsByTagName("dbo:genre");
                
                String ref_st = nome_banda;
                
                for (int temp = 0; temp < nList.getLength(); temp++) {
                    Node nNode = nList.item(temp);
                    Node parent = nList.item(temp).getParentNode();
                    Element parent_el = (Element) parent;
                    String p = parent_el.getAttribute("rdf:about");
                    p = p.replace("http://dbpedia.org/resource/", "");
                    
                    if (nNode.getNodeType() == Node.ELEMENT_NODE && p.equals(ref_st)) {
                        Element eElement = (Element) nNode;
                        String a = eElement.getAttribute("rdf:resource");
                        a = a.replace("http://dbpedia.org/resource/", "");
                        a = a.replace("_", " ");
                        genres.add(a);
                    }
                }
            } catch (Exception e) {
                genres=null;
            }
            //CIDADE DA BANDA

            try {
                nList = band.getElementsByTagName("dbo:hometown");
                hometown = "";
                for (int temp = 0; temp == 0; temp++) {
                    
                    Node nNode = nList.item(temp);
                    
                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        
                        Element eElement = (Element) nNode;
                        String a = eElement.getAttribute("rdf:resource");
                        a = a.replace("http://dbpedia.org/resource/", "");
                        a = a.replace("_", " ");
                        city = a;
                        hometown = a;
                    }
                }
            } catch (Exception e) {
                city=null;
                hometown=null;
            }
            //País da banda
            if (hometown == null) {
                country =null;
            } else {
                try {
                    hometown = hometown.replace(" ", "_");
                    hometown = "http://dbpedia.org/data/" + hometown + ".rdf";
                    String xml_cidade = Extract_artista.crunchifyGetURLContents(hometown);
                    DocumentBuilderFactory factory_cidade = DocumentBuilderFactory.newInstance();
                    DocumentBuilder builder_cidade = factory_cidade.newDocumentBuilder();
                    Document cidade = builder_cidade.parse(new InputSource(new StringReader(xml_cidade)));

                    cidade.getDocumentElement().normalize();

                    NodeList nList_cidade = cidade.getElementsByTagName("dbo:country");

                    String pais_r = city.replace(" ", "_");

                    for (int temp = 0; temp < nList_cidade.getLength(); temp++) {

                        Node nNode = nList_cidade.item(temp);
                        Node parent = nList_cidade.item(temp).getParentNode();
                        Element parent_el = (Element) parent;
                        String p = parent_el.getAttribute("rdf:about");
                        p = p.replace("http://dbpedia.org/resource/", "");

                        if (nNode.getNodeType() == Node.ELEMENT_NODE && p.equals(pais_r)) {
                            Element eElement = (Element) nNode;
                            String a = eElement.getAttribute("rdf:resource");
                            a = a.replace("http://dbpedia.org/resource/", "");
                            a = a.replace("_", " ");
                            country = a;

                        }
                    }

                } catch (Exception e) {
                      country=null;
                }

            }
                try{                          
                String aux_nome=nome_frmt;
                aux_nome=aux_nome.replace("!","");
                aux_nome=aux_nome.replace(".","");
                aux_nome=aux_nome.replace("/","-");
                aux_nome=aux_nome.replace(" (band)","");
                aux_nome=aux_nome.replace(" (DJ)","");
                aux_nome=aux_nome.replace(" (musician)","");
                aux_nome=aux_nome.replace(" (singer)","");
                aux_nome=aux_nome.replace(" (group)","");
                aux_nome=aux_nome.replace(" (Japanese band)","");
                aux_nome=aux_nome.replace(" (Japanese musician, born 1987)","");
                aux_nome=aux_nome.replace(" (music producer)","");
                aux_nome=aux_nome.replace(" (violinist)","");
                aux_nome=aux_nome.replace(" (entertainer)","");
                aux_nome=aux_nome.toLowerCase();
                aux_nome=aux_nome.replace(" ","-");
                System.out.println(aux_nome);
                URL vaga_api = new URL("https://www.vagalume.com.br/"+aux_nome+"/index.js");
                BufferedReader in = new BufferedReader(new InputStreamReader(vaga_api.openStream()));
                String vaga = in.readLine();
                JSONObject vaga_json = new JSONObject(vaga);
                int rk=vaga_json.getJSONObject("artist").getJSONObject("rank").getInt("pos");
                rank= String.valueOf(rk);

                }catch (Exception ex) {
                    rank="indefinido";
                }
        } catch (Exception e) {
        }
    }


        public static String crunchifyGetURLContents(String myURL) {
      //  System.out.println("crunchifyGetURLContents() is hitting : " + myURL);
        StringBuilder sb = new StringBuilder();
        URLConnection urlConn = null;
        InputStreamReader in = null;
        try {
            URL url = new URL(myURL);
            urlConn = url.openConnection();
            if (urlConn != null) {
                urlConn.setReadTimeout(60 * 1000);
            }
            if (urlConn != null && urlConn.getInputStream() != null) {
                in = new InputStreamReader(urlConn.getInputStream(), Charset.defaultCharset());
                BufferedReader bufferedReader = new BufferedReader(in);
                if (bufferedReader != null) {
                    int cp;
                    while ((cp = bufferedReader.read()) != -1) {
                        sb.append((char) cp);
                    }
                    bufferedReader.close();
                }
            }
            in.close();
        } catch (Exception e) {
            throw new RuntimeException("Exception while calling URL:" + myURL, e);
        }

        return sb.toString();
    }
}

