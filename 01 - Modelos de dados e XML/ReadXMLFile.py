#!/usr/bin/python

from xml.dom.minidom import parse
import xml.dom.minidom
import csv

# Open XML document using minidom parser
DOMTree = xml.dom.minidom.parse("marvel_simplificado.xml")




universe = DOMTree.documentElement
if universe.hasAttribute("name"):
   print ("Root element : %s" % universe.getAttribute("name"))

# Get all the heroes in the universe
heroes = universe.getElementsByTagName("hero")

# Print detail of each hero.
N_good_heroes=0
N_bad_heroes=0
Peso=0
nmr_heroes=0

with open('dadosMarvel/herois.csv', mode='w') as herois_file:
   hero_writer = csv.writer(herois_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

   with open('dadosMarvel/herois_good.csv', mode='w') as herois_file:
      hero_writer_good = csv.writer(herois_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

      with open('dadosMarvel/herois_bad.csv', mode='w') as herois_file:
         hero_writer_bad = csv.writer(herois_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
   
         for hero in heroes:
            print ("*****Hero*****")
            if hero.hasAttribute("id"):
               print ("Id: %s" % hero.getAttribute("id"))
               
               name = hero.getElementsByTagName('name')[0]
               print ("Name: %s" % name.childNodes[0].data)
               
               popularity = hero.getElementsByTagName('popularity')[0]
               
               alignment = hero.getElementsByTagName('alignment')[0]
               
               gender = hero.getElementsByTagName('gender')[0]
               
               height_m = hero.getElementsByTagName('height_m')[0]
               
               weight_kg = hero.getElementsByTagName('weight_kg')[0]
               
               hometown = hero.getElementsByTagName('hometown')[0]
               
               intelligence = hero.getElementsByTagName('intelligence')[0]
               
               strength = hero.getElementsByTagName('strength')[0]
               
               speed = hero.getElementsByTagName('speed')[0]
               
               durability = hero.getElementsByTagName('durability')[0]

               energy_Projection = hero.getElementsByTagName('energy_Projection')[0]
               
               fighting_Skills = hero.getElementsByTagName('fighting_Skills')[0]

               Peso=Peso+int(weight_kg.childNodes[0].data)
               nmr_heroes+=1
               
               hero_writer.writerow([hero.getAttribute("id"),name.childNodes[0].data,popularity.childNodes[0].data,alignment.childNodes[0].data,gender.childNodes[0].data,height_m.childNodes[0].data,weight_kg.childNodes[0].data,hometown.childNodes[0].data,intelligence.childNodes[0].data,strength.childNodes[0].data,speed.childNodes[0].data,durability.childNodes[0].data,energy_Projection.childNodes[0].data,fighting_Skills.childNodes[0].data])

               if (name.childNodes[0].data=="Hulk") :
                  peso_hulk=int(weight_kg.childNodes[0].data)
                  altura_hulk=int(height_m.childNodes[0].data)
                  massa_corporal_hulk=peso_hulk/(altura_hulk*altura_hulk)
               
               if (alignment.childNodes[0].data=="Good") :
                  hero_writer_good.writerow([hero.getAttribute("id"),name.childNodes[0].data,popularity.childNodes[0].data,alignment.childNodes[0].data,gender.childNodes[0].data,height_m.childNodes[0].data,weight_kg.childNodes[0].data,hometown.childNodes[0].data,intelligence.childNodes[0].data,strength.childNodes[0].data,speed.childNodes[0].data,durability.childNodes[0].data,energy_Projection.childNodes[0].data,fighting_Skills.childNodes[0].data])
                  N_good_heroes+=1

               if (alignment.childNodes[0].data=="Bad") :
                  hero_writer_bad.writerow([hero.getAttribute("id"),name.childNodes[0].data,popularity.childNodes[0].data,alignment.childNodes[0].data,gender.childNodes[0].data,height_m.childNodes[0].data,weight_kg.childNodes[0].data,hometown.childNodes[0].data,intelligence.childNodes[0].data,strength.childNodes[0].data,speed.childNodes[0].data,durability.childNodes[0].data,energy_Projection.childNodes[0].data,fighting_Skills.childNodes[0].data])
                  N_bad_heroes+=1
            
         media_peso=Peso/nmr_heroes
         
         print("\nProporcao de herois bons para mals eh de : ",N_good_heroes,"/",N_bad_heroes, sep='')
         
         print("\nA media de peso dos herois eh:",media_peso)
         
         print("\nO indice de massa corporal do hulk eh:",massa_corporal_hulk)
         
