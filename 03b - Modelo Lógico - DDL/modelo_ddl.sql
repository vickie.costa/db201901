CREATE TABLE Usuario (
 login VARCHAR(100) NOT NULL,
 nome VARCHAR(100) NOT NULL,
 cidade VARCHAR(100) NOT NULL,
 birthday VARCHAR(100) NOT NULL,
 PRIMARY KEY(login)
);

CREATE TABLE Conhece (
 Usua1 VARCHAR(100) NOT NULL,
 Usua2 VARCHAR(100) NOT NULL,
 FOREIGN KEY(Usua1)
 REFERENCES Usuario(login)
 ON DELETE NO ACTION
 ON UPDATE NO ACTION,
 FOREIGN KEY(Usua2)
 REFERENCES Usuario(login)
 ON DELETE NO ACTION
 ON UPDATE NO ACTION
);


CREATE TABLE Bloqueio (
 Usuario VARCHAR(100) NOT NULL,
 Razao_Bloqueio VARCHAR(100) NOT NULL,
 Usuario_block VARCHAR(100) NOT NULL,
 FOREIGN KEY(Usuario)
 REFERENCES Usuario(login)
 ON DELETE NO ACTION
 ON UPDATE NO ACTION, 
 FOREIGN KEY(Usuario_block)
 REFERENCES Usuario(login)
 ON DELETE NO ACTION
 ON UPDATE NO ACTION 
);


CREATE TABLE Artistas (
 pais VARCHAR(100) NOT NULL,
 nomeArtistico VARCHAR(100) NOT NULL,
 generoMusical VARCHAR(100) NOT NULL,
 PRIMARY KEY(nomeArtistico)
);


CREATE TABLE Curte (
 nota INTEGER NOT NULL,
 Usuario VARCHAR(100) NOT NULL,
 Artistas VARCHAR(100) NOT NULL,
 FOREIGN KEY(Usuario)
 REFERENCES Usuario(login)
 ON DELETE NO ACTION
 ON UPDATE NO ACTION,
 FOREIGN KEY(Artistas)
 REFERENCES Artistas(nomeArtistico)
 ON DELETE NO ACTION
 ON UPDATE NO ACTION
);

CREATE TABLE Musico (
 nomeReal VARCHAR(100) NOT NULL,
 estiloMusical VARCHAR(100) NOT NULL,
 dataNascimento DATE NOT NULL,
 PRIMARY KEY(nomeReal)
);

CREATE TABLE Cantores (
 nomeArtistico VARCHAR(100) NOT NULL,
 nomeReal VARCHAR(100) NOT NULL,
 FOREIGN KEY(nomeArtistico)
 REFERENCES Artistas(nomeArtistico)
 ON DELETE NO ACTION
 ON UPDATE NO ACTION,
 FOREIGN KEY(nomeReal)
 REFERENCES Musico(nomeReal)
 ON DELETE NO ACTION
 ON UPDATE NO ACTION
);



CREATE TABLE Duplas_possui (
 nomeArtistico VARCHAR(100) NOT NULL,
 nomeReal VARCHAR(100) NOT NULL,
 FOREIGN KEY(nomeArtistico)
 REFERENCES Artistas(nomeArtistico)
 ON DELETE NO ACTION
 ON UPDATE NO ACTION,
 FOREIGN KEY(nomeReal)
 REFERENCES Musico(nomeReal)
 ON DELETE NO ACTION
 ON UPDATE NO ACTION
);



CREATE TABLE Grupos_possui (
 nomeArtistico VARCHAR(100) NOT NULL,
 nomeReal VARCHAR(100) NOT NULL,
 FOREIGN KEY(nomeArtistico)
 REFERENCES Artistas(nomeArtistico)
 ON DELETE NO ACTION
 ON UPDATE NO ACTION,
 FOREIGN KEY(nomeReal)
 REFERENCES Musico(nomeReal)
 ON DELETE NO ACTION
 ON UPDATE NO ACTION
);

CREATE TABLE Filme (
 nome VARCHAR(100) NOT NULL,
 dataLancamento DATE NOT NULL,
 PRIMARY KEY(nome)
);

CREATE TABLE Curte2(
 nota INTEGER NOT NULL,
 Usuario VARCHAR(100) NOT NULL,
 Filme VARCHAR(100) NOT NULL,
 FOREIGN KEY(Usuario)
 REFERENCES Usuario(login)
 ON DELETE NO ACTION
 ON UPDATE NO ACTION,
 FOREIGN KEY(Filme)
 REFERENCES Filme(nome)
 ON DELETE NO ACTION
 ON UPDATE NO ACTION
);

CREATE TABLE Categoria (
 desc_categoria VARCHAR(1000) NOT NULL,
 PRIMARY KEY(desc_categoria)
);

CREATE TABLE Classifica (
 nome VARCHAR(100) NOT NULL,
 categoria VARCHAR(100) NOT NULL,
 FOREIGN KEY(nome)
 REFERENCES Filme(nome)
 ON DELETE NO ACTION
 ON UPDATE NO ACTION,
 FOREIGN KEY(categoria)
 REFERENCES Categoria(desc_categoria)
 ON DELETE NO ACTION
 ON UPDATE NO ACTION
);



CREATE TABLE Subcategoria (
 Categ1 VARCHAR(100) NOT NULL,
 Categ2 VARCHAR(100) NOT NULL,
 FOREIGN KEY(Categ1)
 REFERENCES Categoria(desc_categoria)
 ON DELETE NO ACTION
 ON UPDATE NO ACTION,
 FOREIGN KEY(Categ2)
 REFERENCES Categoria(desc_categoria)
 ON DELETE NO ACTION
 ON UPDATE NO ACTION
);

CREATE TABLE Diretor (
 id VARCHAR(100) NOT NULL,
 telefone VARCHAR(100) NOT NULL,
 endereco VARCHAR(100) NOT NULL,
 PRIMARY KEY(id)
);

CREATE TABLE Dirige (
 salarioAssociado VARCHAR(100) NOT NULL,
 id VARCHAR(100) NOT NULL,
 nomeFilme VARCHAR(100) NOT NULL,
 FOREIGN KEY(id)
 REFERENCES Diretor(id)
 ON DELETE NO ACTION
 ON UPDATE NO ACTION,
 FOREIGN KEY(nomeFilme)
 REFERENCES Filme(nome)
 ON DELETE NO ACTION
 ON UPDATE NO ACTION
);

CREATE TABLE Ator (
 id VARCHAR(100) NOT NULL,
 telefone VARCHAR(100) NOT NULL,
 endereco VARCHAR(100) NOT NULL,
 PRIMARY KEY(id)
);

CREATE TABLE Possui2 (
 id VARCHAR(100) NOT NULL,
 nomeFilme VARCHAR(100) NOT NULL,
 salarioAssociado VARCHAR(100) NOT NULL,
 FOREIGN KEY(id)
 REFERENCES Ator(id)
 ON DELETE NO ACTION
 ON UPDATE NO ACTION,
 FOREIGN KEY(nomeFilme)
 REFERENCES Filme(nome)
 ON DELETE NO ACTION
 ON UPDATE NO ACTION
);

INSERT INTO Usuario VALUES('email_1@email.com', 'Adebaldo', 'Curitiba');
INSERT INTO Usuario VALUES('email_2@email.com', 'Fulana', 'Sao Paulo');
INSERT INTO Usuario VALUES('email_3@email.com', 'Siclana', 'Anapolis');
INSERT INTO Usuario VALUES('email_4@email.com', 'Fulano', 'Palmas');
INSERT INTO Usuario VALUES('email_5@email.com', 'Luciana', 'Pato Branco');
INSERT INTO Usuario VALUES('email_6@email.com', 'Rogisnei', 'Townsville');

INSERT INTO Conhece VALUES('email_1@email.com','email_2@email.com');
INSERT INTO Conhece VALUES('email_2@email.com','email_3@email.com');
INSERT INTO Conhece VALUES('email_3@email.com','email_4@email.com');
INSERT INTO Conhece VALUES('email_4@email.com','email_5@email.com');
INSERT INTO Conhece VALUES('email_5@email.com','email_6@email.com');
INSERT INTO Conhece VALUES('email_6@email.com','email_1@email.com');

INSERT INTO Bloqueio VALUES('email_1@email.com','Agressividade','email_6@email.com'); 
INSERT INTO Bloqueio VALUES('email_2@email.com','Spam','email_3@email.com');			
INSERT INTO Bloqueio VALUES('email_3@email.com','Conte�do Impr�prio','email_1@email.com');
INSERT INTO Bloqueio VALUES('email_4@email.com','Fraude','email_1@email.com');			
INSERT INTO Bloqueio VALUES('email_5@email.com','Viol�ncia','email_3@email.com');			
INSERT INTO Bloqueio VALUES('email_6@email.com','Preconceito','email_5@email.com');		


INSERT INTO Filme VALUES('The Hunger Games', '2012-03-23');
INSERT INTO Filme VALUES('Real Steel','2011-10-21');
INSERT INTO Filme VALUES('Killer Bean','2009-12-09');
INSERT INTO Filme VALUES('O Auto da Compadecida','2000-09-10');
INSERT INTO Filme VALUES('As trancas do rei careca','2000-02-29');
INSERT INTO Filme VALUES('A volta dos que nao foram','1975-12-2');

INSERT INTO Curte2 VALUES('9', 'email_1@email.com', 'The Hunger Games');
INSERT INTO Curte2 VALUES('7', 'email_1@email.com' ,'Real Steel' );
INSERT INTO Curte2 VALUES('8', 'email_3@email.com', 'The Hunger Games');
INSERT INTO Curte2 VALUES('10', 'email_5@email.com', 'O Auto da Compadecida');
INSERT INTO Curte2 VALUES('8', 'email_2@email.com', 'O Auto da Compadecida');
INSERT INTO Curte2 VALUES('9', 'email_4@email.com', 'As trancas do rei careca');

INSERT INTO Diretor VALUES('Diretor1', '98874-8774', 'Rua judas perdeu as botas 201');
INSERT INTO Diretor VALUES('Diretor2', '98874-1274', 'Rua judas perdeu as botas 22');
INSERT INTO Diretor VALUES('Diretor3', '98854-7774', 'Rua judas perdeu as botas 25');

INSERT INTO Dirige VALUES('2000','Diretor1','O Auto da Compadecida');
INSERT INTO Dirige VALUES('10000','Diretor2','The Hunger Games');
INSERT INTO Dirige VALUES('5000','Diretor3','Real Steel');
INSERT INTO Dirige VALUES('1000','Diretor1','As trancas do rei careca');
INSERT INTO Dirige VALUES('6000','Diretor2','A volta dos que nao foram');
INSERT INTO Dirige VALUES('10000','Diretor3','Killer Bean');

INSERT INTO Categoria VALUES('Romance');
INSERT INTO Categoria VALUES('Acao');
INSERT INTO Categoria VALUES('Ficcao');
INSERT INTO Categoria VALUES('Terror');
INSERT INTO Categoria VALUES('Comedia');
INSERT INTO Categoria VALUES('Cyberpunk');

INSERT INTO Classifica VALUES('O Auto da Compadecida','Romance');
INSERT INTO Classifica VALUES('The Hunger Games','Acao');
INSERT INTO Classifica VALUES('Real Steel','Ficcao');
INSERT INTO Classifica VALUES('A volta dos que nao foram','Cyberpunk');
INSERT INTO Classifica VALUES('Killer Bean','Comedia');
INSERT INTO Classifica VALUES('As trancas do rei careca','Comedia');

INSERT INTO Subcategoria VALUES('Acao','Ficcao');
INSERT INTO Subcategoria VALUES('Ficcao','Cyberpunk');
INSERT INTO Subcategoria VALUES('Romance','Comedia');

INSERT INTO Ator VALUES('Ator1','95555-5555','Rua Judas perdeu as meias 203');
INSERT INTO Ator VALUES('Ator2','95556-5555','Rua Judas perdeu as meias 205');
INSERT INTO Ator VALUES('Ator3','95557-5555','Rua Judas perdeu as meias 206');
INSERT INTO Ator VALUES('Ator4','95558-5555','Rua Judas perdeu as meias 22');
INSERT INTO Ator VALUES('Ator5','95559-5555','Rua Judas perdeu as meias 207');
INSERT INTO Ator VALUES('Ator6','95551-0555','Rua Judas perdeu as meias 27');

INSERT INTO Possui2 VALUES('Ator1','O Auto da Compadecida','2500');
INSERT INTO Possui2 VALUES('Ator2','The Hunger Games','3200');
INSERT INTO Possui2 VALUES('Ator3','Real Steel','1500');
INSERT INTO Possui2 VALUES('Ator1','Killer Bean','4200');
INSERT INTO Possui2 VALUES('Ator3','A volta dos que nao foram','200');
INSERT INTO Possui2 VALUES('Ator5','As trancas do rei careca','2890');

INSERT INTO Artistas VALUES('Brasil', 'Artist1', 'PopRock');
INSERT INTO Artistas VALUES('EUA',    'Artist2', 'EletroFunk');
INSERT INTO Artistas VALUES('Espanha','Artist3', 'Heavy Metal');
INSERT INTO Artistas VALUES('Brasil', 'Artist4', 'Slash Metal');
INSERT INTO Artistas VALUES('Mexico', 'Artist5', 'Axe');
INSERT INTO Artistas VALUES('Brasil', 'Artist6', 'Rock alternativo');
INSERT INTO Artistas VALUES('Brasil', 'Artist7', 'Indie');
INSERT INTO Artistas VALUES('Mexico', 'Artist8', 'PopRock');
INSERT INTO Artistas VALUES('Mexico', 'Artist9', 'Samba');
INSERT INTO Artistas VALUES('Brasil', 'Artist10', 'Psy');
INSERT INTO Artistas VALUES('EUA',    'Artist11', 'Punk Rock');
INSERT INTO Artistas VALUES('Brasil', 'Artist12', 'EletroPunk');

INSERT INTO Curte VALUES('10', 'email_1@email.com', 'Artist1');
INSERT INTO Curte VALUES('6', 'email_3@email.com', 'Artist2');
INSERT INTO Curte VALUES('9', 'email_1@email.com', 'Artist4');
INSERT INTO Curte VALUES('8', 'email_2@email.com', 'Artist3');
INSERT INTO Curte VALUES('8', 'email_4@email.com', 'Artist1');
INSERT INTO Curte VALUES('10', 'email_2@email.com', 'Artist5');

INSERT INTO Musico VALUES('Jose da Silva','POP','01/02/1950');
INSERT INTO Musico VALUES('Carla pereira','Eletronica','05/04/1985');
INSERT INTO Musico VALUES('luciele dias','Metal','06/07/1975');
INSERT INTO Musico VALUES('luana nascimento da silva','Metal','06/11/1973');
INSERT INTO Musico VALUES('jo�o carvalho furtado','Samba','02/02/1987');
INSERT INTO Musico VALUES('alberto gordinsk','Samba','08/05/1968');
INSERT INTO Musico values ('Ana Clara josua','PopRock','07/03/92');					
INSERT INTO Musico values ('Jose rodolfo','PopRock'	,'07/07/1985');
INSERT INTO Musico values ('adolfo ricardo','Pagode','13/09/1991');
INSERT INTO Musico values ('Rodrigo consuelo','Pagode','27/11/1977');							
INSERT INTO Musico values ('Karen Liande ricard','Axe','04/18/1989');
INSERT INTO Musico values ('Onaha liandrez','Axe','05/08/1989');
INSERT INTO Musico VALUES('Jose aparecido da cruz','Rock','08/05/1968');
INSERT INTO Musico VALUES('Alberto ferreira costa','Rock','08/05/1968');
INSERT INTO Musico VALUES('Janini silvana fernandez' ,'Rock', '05/10/1990');




INSERT INTO Cantores VALUES('Artist1','Jose da Silva');
INSERT INTO Cantores VALUES('Artist2','Carla pereira');
INSERT INTO Cantores VALUES('Artist3','luciele dias');
INSERT INTO Cantores VALUES('Artist4','luana nascimento da silva');
INSERT INTO Cantores VALUES('Artist5','jo�o carvalho furtado');
INSERT INTO Cantores VALUES('Artist6','alberto gordinsk') ;


INSERT INTO Duplas_Possui VALUES('Artist7','Ana Clara josua');
INSERT INTO Duplas_Possui VALUES('Artist7','Jose rodolfo');
INSERT INTO Duplas_Possui VALUES('Artist8','adolfo ricardo');
INSERT INTO Duplas_Possui VALUES('Artist8','Rodrigo consuelo');
INSERT INTO Duplas_Possui VALUES('Artist9','Karen Liande ricard');
INSERT INTO Duplas_Possui VALUES('Artist9','Onaha liandrez');

Insert into Grupos_possui VALUES ('Artist10','Jose aparecido da cruz');
Insert into Grupos_possui VALUES ('Artist10','Alberto ferreira costa');
Insert into Grupos_possui VALUES ('Artist10','Janini silvana fernandez');
