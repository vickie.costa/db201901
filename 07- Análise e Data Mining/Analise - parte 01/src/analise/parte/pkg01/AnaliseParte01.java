/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analise.parte.pkg01;

import java.awt.Color;
import javax.swing.JFrame;
import de.erichseifert.gral.data.DataTable;
import de.erichseifert.gral.graphics.Label;
import de.erichseifert.gral.plots.PlotArea;
import de.erichseifert.gral.plots.XYPlot;
import de.erichseifert.gral.plots.lines.DefaultLineRenderer2D;
import de.erichseifert.gral.plots.lines.LineRenderer;
import de.erichseifert.gral.plots.points.PointRenderer;
import de.erichseifert.gral.ui.InteractivePanel;


/**
 *
 * @author luizcelso
 */
public class AnaliseParte01 extends JFrame {

    public AnaliseParte01(DataTable data) {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(600, 400);


        XYPlot plot = new XYPlot(data);
        getContentPane().add(new InteractivePanel(plot));
        LineRenderer lines = new DefaultLineRenderer2D();
        plot.setLineRenderers(data, lines);
        Color color = new Color(0.0f, 0.3f, 1.0f);
        plot.getPointRenderers(data); //.setColor(color);
        plot.getLineRenderers(data); //.setColor(color);
        
        // Style the plot area
        plot.getPlotArea().setBorderColor(new Color(0.0f, 0.3f, 1.0f));
        //plot.getPlotArea().setBorderStroke(new Stroke(2f));

        
        // Style axes 
        plot.getAxisRenderer(XYPlot.AXIS_X).setLabel(new Label("X"));
        plot.getAxisRenderer(XYPlot.AXIS_Y).setLabel(new Label("Y"));
        plot.getAxisRenderer(XYPlot.AXIS_X).setTickSpacing(1.0);
        plot.getAxisRenderer(XYPlot.AXIS_Y).setTickSpacing(2.0);
        plot.getAxisRenderer(XYPlot.AXIS_X).setIntersection(-Double.MAX_VALUE);
        plot.getAxisRenderer(XYPlot.AXIS_Y).setIntersection(-Double.MAX_VALUE);
    }

    public static void main(String[] args) {


    }
    
}
