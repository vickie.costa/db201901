
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author andre
 */
public class Principal extends javax.swing.JFrame {

    public String estadoUsuario = "Consultar";
    public String estadoFilme = "Cancelar";
    public String estadoArtista = "Cancelar";
    public String estadoAmigo = "Cancelar";
    
    


    public void LoadTableUsuariosFilme() {

        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql://200.134.10.32:5432/1901VMA", "1901VMA", "786333");
            c.setAutoCommit(false);

            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM usuario;");

            DefaultTableModel modelo = new DefaultTableModel(new Object[]{"Login", "Nome", "Cidade", "Data de nascimento"}, 0);

            while (rs.next()) {
                Object linha[] = new Object[]{rs.getString("login"), rs.getString("nome"), rs.getString("cidade"), rs.getString("birthday")};
                modelo.addRow(linha);
            }

            tbl_usuario_filmes.setModel(modelo);
            tbl_usuario_filmes.getColumnModel().getColumn(0).setPreferredWidth(300);
            tbl_usuario_filmes.getColumnModel().getColumn(1).setPreferredWidth(100);
            tbl_usuario_filmes.getColumnModel().getColumn(2).setPreferredWidth(100);
            tbl_usuario_filmes.getColumnModel().getColumn(3).setPreferredWidth(100);

            rs.close();
            stmt.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
            JOptionPane.showMessageDialog(null, e.getClass().getName() + ": " + e.getMessage());
        }
        JOptionPane.showMessageDialog(null, "Tabela atualizada!");

    }
   
    public void LoadTableUsuariosArtista() {

        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql://200.134.10.32:5432/1901VMA", "1901VMA", "786333");
            c.setAutoCommit(false);

            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM usuario;");

            DefaultTableModel modelo = new DefaultTableModel(new Object[]{"Login", "Nome", "Cidade", "Data de nascimento"}, 0);

            while (rs.next()) {
                Object linha[] = new Object[]{rs.getString("login"), rs.getString("nome"), rs.getString("cidade"), rs.getString("birthday")};
                modelo.addRow(linha);
            }

            tbl_usuario_musicas.setModel(modelo);
            tbl_usuario_musicas.getColumnModel().getColumn(0).setPreferredWidth(300);
            tbl_usuario_musicas.getColumnModel().getColumn(1).setPreferredWidth(100);
            tbl_usuario_musicas.getColumnModel().getColumn(2).setPreferredWidth(100);
            tbl_usuario_musicas.getColumnModel().getColumn(3).setPreferredWidth(100);

            rs.close();
            stmt.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
            JOptionPane.showMessageDialog(null, e.getClass().getName() + ": " + e.getMessage());
        }
        JOptionPane.showMessageDialog(null, "Tabela atualizada!");

    }


    public void ListaFilme() {

        if (tbl_usuario_filmes.getSelectedRow() >= 0) {
            int row = tbl_usuario_filmes.getSelectedRow();
            String usu = tbl_usuario_filmes.getModel().getValueAt(row, 0).toString();

            Connection c = null;
            Statement stmt = null;
            try {
                Class.forName("org.postgresql.Driver");
                c = DriverManager.getConnection("jdbc:postgresql://200.134.10.32:5432/1901VMA", "1901VMA", "786333");
                c.setAutoCommit(false);
          
                stmt = c.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                ResultSet rs = stmt.executeQuery("WITH Generos_preferidos as (SELECT c2.usuario,cl.categoria,COUNT(*) FROM curte2 c2,classifica cl \n" +
                    "WHERE c2.filme = cl.nome AND c2.usuario='" + usu + "'\n" +
                    "GROUP BY c2.usuario,cl.categoria\n" +
                    "ORDER BY COUNT(*) DESC\n" +
                    "LIMIT 3)\n" +
                    "\n" +
                    "Select distinct nm.nome,cl.nome AS uri_movie FROM Classifica cl,name_movie nm\n" +
                    "Where nm.uri_movie=cl.nome and categoria in(Select categoria FROM Generos_preferidos) AND cl.nome NOT IN(SELECT filme FROM curte2 where usuario='" + usu + "')\n"
                    + "ORDER BY nm.nome");

                DefaultTableModel modelo = new DefaultTableModel(new Object[]{"Nome", "URI"}, 0);
     
               if (!(rs.next()))
                {
                    rs = stmt.executeQuery("WITH Generos_preferidos as (SELECT c2.filme, COUNT(*) FROM curte2 c2 \n" +
                                            "GROUP BY c2.filme\n" +
                                            "ORDER BY COUNT(*) DESC\n" +
                                            "LIMIT 20\n" +
                                            ")\n" +
                                            "\n" +
                                            "Select distinct nm.nome,gp.filme as Uri_movie  FROM Generos_preferidos gp, name_movie nm\n" +
                                            "where nm.uri_movie=gp.filme\n" +
                                            "ORDER BY nm.nome");

                    
                }  
               rs.beforeFirst();
                while (rs.next()) { 
                    Object linha[] = new Object[]{rs.getString("nome"), rs.getString("uri_movie")};
                    modelo.addRow(linha);
                }

                tbl_gosta_filmes.setModel(modelo);
                tbl_gosta_filmes.getColumnModel().getColumn(0).setPreferredWidth(300);
                tbl_gosta_filmes.getColumnModel().getColumn(1).setPreferredWidth(100);
                
                rs.close();
                stmt.close();
                c.close();
            } catch (Exception e) {
                System.err.println(e.getClass().getName() + ": " + e.getMessage());
                System.exit(0);
                JOptionPane.showMessageDialog(null, e.getClass().getName() + ": " + e.getMessage());
            }
            JOptionPane.showMessageDialog(null, "Tabela de filmes atualizada!");

        } else {
            JOptionPane.showMessageDialog(null, "Selecione um usuário.");
        }

    }
   
    public void ListaArtista() {

        if (tbl_usuario_musicas.getSelectedRow() >= 0) {

            int row = tbl_usuario_musicas.getSelectedRow();
            String usu = tbl_usuario_musicas.getModel().getValueAt(row, 0).toString();

            Connection c = null;
            Statement stmt = null;
            try {
                Class.forName("org.postgresql.Driver");
                c = DriverManager.getConnection("jdbc:postgresql://200.134.10.32:5432/1901VMA", "1901VMA", "786333");
                c.setAutoCommit(false);

               stmt = c.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                ResultSet rs = stmt.executeQuery("WITH Estilos_preferidos as (SELECT c1.usuario,cb.estilo,COUNT(*) FROM curte c1,class_band cb \n" +
                    "WHERE c1.artistas = cb.uri_band AND c1.usuario='"+usu+"'\n" +
                    "GROUP BY c1.usuario,cb.estilo\n" +
                    "ORDER BY COUNT(*) DESC\n" +
                    "LIMIT 3)\n" +
                    "\n" +
                    "Select distinct nb.nome,cb.uri_band FROM class_band cb,name_band nb\n" +
                    "Where nb.uri_band=cb.uri_band and cb.estilo in(Select ep.estilo FROM Estilos_preferidos ep) AND cb.uri_band NOT IN(SELECT artistas FROM curte where usuario='"+usu+"')");

                DefaultTableModel modelo = new DefaultTableModel(new Object[]{"Nome da Banda", "URI"}, 0);
     
               if (!(rs.next()))
                {
                    rs = stmt.executeQuery("WITH Estilos_preferidos as (SELECT c1.artistas, COUNT(*) FROM curte c1 \n" +
                                            "GROUP BY c1.artistas\n" +
                                            "ORDER BY COUNT(*) DESC\n" +
                                            "LIMIT 20\n" +
                                            ")\n" +
                                            "\n" +
                                            "Select distinct nb.nome,ep.artistas as URI_band FROM Estilos_preferidos ep, name_band nb\n" +
                                            "where nb.uri_band=ep.artistas\n" +
                                            "ORDER BY nb.nome");

                    
                }                 rs.beforeFirst();
                while (rs.next()) {
                    Object linha[] = new Object[]{rs.getString("nome"), rs.getString("uri_band")};
                    modelo.addRow(linha);
                }

                tbl_gosta_musicas.setModel(modelo);
                tbl_gosta_musicas.getColumnModel().getColumn(0).setPreferredWidth(300);
                tbl_gosta_musicas.getColumnModel().getColumn(1).setPreferredWidth(100);
                System.out.println(tbl_gosta_musicas.getRowCount());
                rs.close();
                stmt.close();
                c.close();
            } catch (Exception e) {
                System.err.println(e.getClass().getName() + ": " + e.getMessage());
                System.exit(0);
                JOptionPane.showMessageDialog(null, e.getClass().getName() + ": " + e.getMessage());
            }
            JOptionPane.showMessageDialog(null, "Tabela de artistas atualizada!");

        } else {
            JOptionPane.showMessageDialog(null, "Selecione um usuário.");
        }

    }

    public void CadUsuario(String login, String nome, String cidade, String birthday) {

        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql://200.134.10.32:5432/1901VMA", "1901VMA", "786333");
            stmt = c.createStatement();
            String AddUsuario = "INSERT INTO usuario VALUES('" + login + "','" + nome + "','" + cidade + "','" + birthday + "')";
            stmt.executeUpdate(AddUsuario);
            stmt.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            JOptionPane.showMessageDialog(null, e.getClass().getName() + ": " + e.getMessage());
        }
        JOptionPane.showMessageDialog(null, "Operation done successfully");

    }

    public void DelUsuario(String login) {

        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql://200.134.10.32:5432/1901VMA", "1901VMA", "786333");
            stmt = c.createStatement();
            String DelUsuario = "DELETE FROM conhece WHERE usua1='" + login + "' or usua2='" + login + "';"
                    + "DELETE FROM curte WHERE usuario='" + login + "';"
                    + "DELETE FROM curte2 WHERE usuario='" + login + "';"
                    + "DELETE FROM bloqueio WHERE usuario='" + login + "' or usuario_block='" + login + "';"
                    + "DELETE FROM usuario WHERE login='" + login + "';";

            stmt.executeUpdate(DelUsuario);
            stmt.close();

            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
            JOptionPane.showMessageDialog(null, e.getClass().getName() + ": " + e.getMessage());
        }
        JOptionPane.showMessageDialog(null, "Operation done successfully");

    }

    public void EditUsuario(String login, String nome, String cidade, String birthday) {
        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql://200.134.10.32:5432/1901VMA", "1901VMA", "786333");
            stmt = c.createStatement();
            String EditUsuario = "UPDATE usuario SET nome='" + nome
                    + "',cidade='" + cidade
                    + "',birthday='" + birthday + "'"
                    + "WHERE login='" + login + "';";

            stmt.executeUpdate(EditUsuario);
            stmt.close();

            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
            JOptionPane.showMessageDialog(null, e.getClass().getName() + ": " + e.getMessage());
        }
        JOptionPane.showMessageDialog(null, "Operation done successfully");

    }

    public void ConheceUsuario(String usuario, String amigo) {

        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql://200.134.10.32:5432/1901VMA", "1901VMA", "786333");
            stmt = c.createStatement();
            String AddAmigo = "INSERT INTO conhece VALUES('" + usuario + "','" + amigo + "');";
            stmt.executeUpdate(AddAmigo);
            stmt.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
            JOptionPane.showMessageDialog(null, e.getClass().getName() + ": " + e.getMessage());
        }
        JOptionPane.showMessageDialog(null, "Operation done successfully");
    }

    public void CurteFilme(String usuario, String filme, int nota) {

        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql://200.134.10.32:5432/1901VMA", "1901VMA", "786333");
            stmt = c.createStatement();

            //Add o filme caso ainda nao esteja no banco de dados
            try {
                String AddFilme = "INSERT INTO filme VALUES('" + filme + "','1901-12-12')";
                stmt.executeUpdate(AddFilme);
            } catch (Exception e) {
                System.err.println(e.getClass().getName() + ": " + e.getMessage());
            }
            //Add o filme aos gostos
            try {
                String AddCurteFilme = "INSERT INTO curte2 VALUES('" + nota + "','" + usuario + "','" + filme + "')";
                stmt.executeUpdate(AddCurteFilme);

            } catch (Exception e) {
                System.err.println(e.getClass().getName() + ": " + e.getMessage());
                System.exit(0);
                JOptionPane.showMessageDialog(null, e.getClass().getName() + ": " + e.getMessage());
            }
            stmt.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
            JOptionPane.showMessageDialog(null, e.getClass().getName() + ": " + e.getMessage());
        }

        JOptionPane.showMessageDialog(null, "Operation done successfully");

    }

    public void ExcluiFilme() {

        if (tbl_gosta_filmes.getSelectedRow() >= 0) {
            
            int row = tbl_gosta_filmes.getSelectedRow();
            String filme = tbl_gosta_filmes.getModel().getValueAt(row, 0).toString();

            Connection c = null;
            Statement stmt = null;
            try {
                Class.forName("org.postgresql.Driver");
                c = DriverManager.getConnection("jdbc:postgresql://200.134.10.32:5432/1901VMA", "1901VMA", "786333");
                stmt = c.createStatement();
                String DelFilme = "DELETE FROM curte2 WHERE filme='"+filme+"'";

                stmt.executeUpdate(DelFilme);
                stmt.close();

                c.close();
            } catch (Exception e) {
                System.err.println(e.getClass().getName() + ": " + e.getMessage());
                System.exit(0);
                JOptionPane.showMessageDialog(null, e.getClass().getName() + ": " + e.getMessage());
            }
            JOptionPane.showMessageDialog(null, "Operation done successfully");
        } else {
            JOptionPane.showMessageDialog(null, "Selecione um filme.");
        }

    }
    
    public void ExcluiArtista() {

        if (tbl_gosta_musicas.getSelectedRow() >= 0) {
            
            int row = tbl_gosta_musicas.getSelectedRow();
            String artista = tbl_gosta_musicas.getModel().getValueAt(row, 0).toString();

            Connection c = null;
            Statement stmt = null;
            try {
                Class.forName("org.postgresql.Driver");
                c = DriverManager.getConnection("jdbc:postgresql://200.134.10.32:5432/1901VMA", "1901VMA", "786333");
                stmt = c.createStatement();
                String DelArtista = "DELETE FROM curte WHERE artistas='"+artista+"'";

                stmt.executeUpdate(DelArtista);
                stmt.close();

                c.close();
            } catch (Exception e) {
                System.err.println(e.getClass().getName() + ": " + e.getMessage());
                System.exit(0);
                JOptionPane.showMessageDialog(null, e.getClass().getName() + ": " + e.getMessage());
            }
            JOptionPane.showMessageDialog(null, "Operation done successfully");
        } else {
            JOptionPane.showMessageDialog(null, "Selecione um artista.");
        }

    }
  
    public void CurteMusica(String usuario, String artista, int nota) {

        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql://200.134.10.32:5432/1901VMA", "1901VMA", "786333");
            stmt = c.createStatement();

            //Add o artista caso ainda nao esteja no banco de dados
            try {
                String AddArtista = "INSERT INTO artistas VALUES('PANGEIA','" + artista + "','SURPRESA');";
                stmt.executeUpdate(AddArtista);
            } catch (Exception e) {
                System.err.println(e.getClass().getName() + ": " + e.getMessage());
            }
            //Add o filme aos gostos
            try {
                String AddCurteArtista = "INSERT INTO curte VALUES('" + nota + "','" + usuario + "','" + artista + "');";
                stmt.executeUpdate(AddCurteArtista);

            } catch (Exception e) {
                System.err.println(e.getClass().getName() + ": " + e.getMessage());
                System.exit(0);
                JOptionPane.showMessageDialog(null, e.getClass().getName() + ": " + e.getMessage());
            }
            stmt.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
            JOptionPane.showMessageDialog(null, e.getClass().getName() + ": " + e.getMessage());
        }

        JOptionPane.showMessageDialog(null, "Operation done successfully");

    }

    /**
     * Creates new form Principal
     */
    public Principal() {
        initComponents();
        setLocationRelativeTo(null);

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tbl_gosta_filmes = new javax.swing.JTable();
        jScrollPane5 = new javax.swing.JScrollPane();
        tbl_usuario_filmes = new javax.swing.JTable();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        btn_listarusu_filmes = new javax.swing.JButton();
        btn_listarfilm_filmes = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        tbl_gosta_musicas = new javax.swing.JTable();
        jScrollPane7 = new javax.swing.JScrollPane();
        tbl_usuario_musicas = new javax.swing.JTable();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        ListUsuMus = new javax.swing.JButton();
        btn_listar_artista = new javax.swing.JButton();

        jMenu1.setText("jMenu1");

        jMenu2.setText("jMenu2");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        tbl_gosta_filmes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nome", "Nota"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane3.setViewportView(tbl_gosta_filmes);
        if (tbl_gosta_filmes.getColumnModel().getColumnCount() > 0) {
            tbl_gosta_filmes.getColumnModel().getColumn(0).setPreferredWidth(300);
            tbl_gosta_filmes.getColumnModel().getColumn(1).setPreferredWidth(100);
        }

        tbl_usuario_filmes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Login", "Nome", "Cidade", "Data de nascimento"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane5.setViewportView(tbl_usuario_filmes);
        if (tbl_usuario_filmes.getColumnModel().getColumnCount() > 0) {
            tbl_usuario_filmes.getColumnModel().getColumn(0).setPreferredWidth(300);
            tbl_usuario_filmes.getColumnModel().getColumn(1).setPreferredWidth(100);
            tbl_usuario_filmes.getColumnModel().getColumn(2).setPreferredWidth(100);
            tbl_usuario_filmes.getColumnModel().getColumn(2).setHeaderValue("Cidade");
            tbl_usuario_filmes.getColumnModel().getColumn(3).setPreferredWidth(100);
            tbl_usuario_filmes.getColumnModel().getColumn(3).setHeaderValue("Data de nascimento");
        }

        jLabel3.setText("Usuário:");

        jLabel4.setText("Recomendações:");

        btn_listarusu_filmes.setText("Listar usuários");
        btn_listarusu_filmes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_listarusu_filmesActionPerformed(evt);
            }
        });

        btn_listarfilm_filmes.setText("Listar filmes");
        btn_listarfilm_filmes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_listarfilm_filmesActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 1000, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btn_listarusu_filmes, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4)
                            .addComponent(btn_listarfilm_filmes, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btn_listarusu_filmes)
                .addGap(34, 34, 34)
                .addComponent(jLabel4)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btn_listarfilm_filmes)
                .addContainerGap(281, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Recomendação de Filme", jPanel3);

        tbl_gosta_musicas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nome", "Nota"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane6.setViewportView(tbl_gosta_musicas);
        if (tbl_gosta_musicas.getColumnModel().getColumnCount() > 0) {
            tbl_gosta_musicas.getColumnModel().getColumn(0).setPreferredWidth(100);
            tbl_gosta_musicas.getColumnModel().getColumn(1).setPreferredWidth(100);
        }

        tbl_usuario_musicas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Login", "Nome", "Cidade", "Data de nascimento"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane7.setViewportView(tbl_usuario_musicas);
        if (tbl_usuario_musicas.getColumnModel().getColumnCount() > 0) {
            tbl_usuario_musicas.getColumnModel().getColumn(0).setPreferredWidth(300);
            tbl_usuario_musicas.getColumnModel().getColumn(1).setPreferredWidth(100);
            tbl_usuario_musicas.getColumnModel().getColumn(2).setPreferredWidth(100);
            tbl_usuario_musicas.getColumnModel().getColumn(3).setPreferredWidth(100);
        }

        jLabel5.setText("Usuário:");

        jLabel6.setText("Recomendações:");

        ListUsuMus.setText("Listar Usuários");
        ListUsuMus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ListUsuMusActionPerformed(evt);
            }
        });

        btn_listar_artista.setText("Listar Artistas");
        btn_listar_artista.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_listar_artistaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane7)
                    .addComponent(jScrollPane6)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6)
                            .addComponent(ListUsuMus)
                            .addComponent(btn_listar_artista, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 840, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 208, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ListUsuMus)
                .addGap(30, 30, 30)
                .addComponent(jLabel6)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_listar_artista)
                .addContainerGap(288, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Recomendação de Músicas", jPanel4);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_listar_artistaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_listar_artistaActionPerformed

        ListaArtista();
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_listar_artistaActionPerformed

    private void ListUsuMusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ListUsuMusActionPerformed
        LoadTableUsuariosArtista();
        // TODO add your handling code here:
    }//GEN-LAST:event_ListUsuMusActionPerformed

    private void btn_listarfilm_filmesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_listarfilm_filmesActionPerformed
        // TODO add your handling code here:

        ListaFilme();
    }//GEN-LAST:event_btn_listarfilm_filmesActionPerformed

    private void btn_listarusu_filmesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_listarusu_filmesActionPerformed
        // TODO add your handling code here:

        LoadTableUsuariosFilme();
    }//GEN-LAST:event_btn_listarusu_filmesActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Principal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ListUsuMus;
    private javax.swing.JButton btn_listar_artista;
    private javax.swing.JButton btn_listarfilm_filmes;
    private javax.swing.JButton btn_listarusu_filmes;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable tbl_gosta_filmes;
    private javax.swing.JTable tbl_gosta_musicas;
    private javax.swing.JTable tbl_usuario_filmes;
    private javax.swing.JTable tbl_usuario_musicas;
    // End of variables declaration//GEN-END:variables
}
