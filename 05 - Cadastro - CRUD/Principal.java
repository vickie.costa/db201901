
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author andre
 */
public class Principal extends javax.swing.JFrame {

    public String estadoUsuario = "Consultar";
    public String estadoFilme = "Cancelar";
    public String estadoArtista = "Cancelar";
    public String estadoAmigo = "Cancelar";
    
    

    public void EstadoUsuario() {
        switch (estadoUsuario) {
            case "Novo":
                btn_salvar_usuarios.setEnabled(true);
                btn_cancelar_usuarios.setEnabled(true);
                txtb_cidade_usuarios.setEnabled(true);
                txtb_data_usuarios.setEnabled(true);
                txtb_login_usuarios.setEnabled(true);
                txtb_nome_usuarios.setEnabled(true);
                break;
            case "Editar":
                btn_salvar_usuarios.setEnabled(true);
                btn_cancelar_usuarios.setEnabled(true);
                txtb_cidade_usuarios.setEnabled(true);
                txtb_data_usuarios.setEnabled(true);
                txtb_login_usuarios.setEnabled(false);
                txtb_nome_usuarios.setEnabled(true);
                break;
            case "Cancelar":
                btn_salvar_usuarios.setEnabled(false);
                btn_cancelar_usuarios.setEnabled(false);
                txtb_cidade_usuarios.setEnabled(false);
                txtb_data_usuarios.setEnabled(false);
                txtb_login_usuarios.setEnabled(false);
                txtb_nome_usuarios.setEnabled(false);
                break;
            case "Consultar":
                btn_salvar_usuarios.setEnabled(false);
                btn_cancelar_usuarios.setEnabled(false);
                txtb_cidade_usuarios.setEnabled(false);
                txtb_data_usuarios.setEnabled(false);
                txtb_login_usuarios.setEnabled(false);
                txtb_nome_usuarios.setEnabled(false);
                break;
            default:
                btn_salvar_usuarios.setEnabled(false);
                btn_cancelar_usuarios.setEnabled(false);
                txtb_cidade_usuarios.setEnabled(false);
                txtb_data_usuarios.setEnabled(false);
                txtb_login_usuarios.setEnabled(false);
                txtb_nome_usuarios.setEnabled(false);

        }
    }

    public void EstadoFilme() {
        switch (estadoFilme) {
            case "Novo":
                btn_add_filme.setEnabled(true);
                btn_cancelar_filme.setEnabled(true);
                txt_nome_filme.setEnabled(true);
                txtb_nota_filme.setEnabled(true);
                break;
            case "Cancelar":
                btn_add_filme.setEnabled(false);
                btn_cancelar_filme.setEnabled(false);
                txt_nome_filme.setEnabled(false);
                txtb_nota_filme.setEnabled(false);
                break;
            default:
                btn_add_filme.setEnabled(false);
                btn_cancelar_filme.setEnabled(false);
                txt_nome_filme.setEnabled(false);
                txtb_nota_filme.setEnabled(false);
                break;

        }
    }
   
    public void EstadoArtista() {
        switch (estadoArtista) {
            case "Novo":
                btn_add_artista.setEnabled(true);
                btn_cancelar_artista.setEnabled(true);
                txt_nome_artista.setEnabled(true);
                txtb_nota_artista.setEnabled(true);
                break;
            case "Cancelar":
                btn_add_artista.setEnabled(false);
                btn_cancelar_artista.setEnabled(false);
                txt_nome_artista.setEnabled(false);
                txtb_nota_artista.setEnabled(false);
                break;
            default:
                btn_add_artista.setEnabled(false);
                btn_cancelar_artista.setEnabled(false);
                txt_nome_artista.setEnabled(false);
                txtb_nota_artista.setEnabled(false);
                break;

        }
    }
   
    public void EstadoAmigo() {
        switch (estadoAmigo) {
            case "Novo":
                btn_add_amigo.setEnabled(true);
                btn_cancelar_amigo.setEnabled(true);
                txt_nome_amigo.setEnabled(true);
                break;
            case "Cancelar":
                btn_add_amigo.setEnabled(false);
                btn_cancelar_amigo.setEnabled(false);
                txt_nome_amigo.setEnabled(false);
                break;
            default:
                btn_add_amigo.setEnabled(false);
                btn_cancelar_amigo.setEnabled(false);
                txt_nome_amigo.setEnabled(false);
                break;

        }
    }

    public void LoadTableUsuarios() {

        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql://200.134.10.32:5432/1901VMA", "1901VMA", "786333");
            c.setAutoCommit(false);

            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM usuario;");

            DefaultTableModel modelo = new DefaultTableModel(new Object[]{"Login", "Nome", "Cidade", "Data de nascimento"}, 0);

            while (rs.next()) {
                Object linha[] = new Object[]{rs.getString("login"), rs.getString("nome"), rs.getString("cidade"), rs.getString("birthday")};
                modelo.addRow(linha);
            }

            tblUsuarios.setModel(modelo);
            tblUsuarios.getColumnModel().getColumn(0).setPreferredWidth(300);
            tblUsuarios.getColumnModel().getColumn(1).setPreferredWidth(100);
            tblUsuarios.getColumnModel().getColumn(2).setPreferredWidth(100);
            tblUsuarios.getColumnModel().getColumn(3).setPreferredWidth(100);

            rs.close();
            stmt.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
            JOptionPane.showMessageDialog(null, e.getClass().getName() + ": " + e.getMessage());
        }
        JOptionPane.showMessageDialog(null, "Tabela atualizada!");

    }

    public void LoadTableUsuariosConhecidos() {

        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql://200.134.10.32:5432/1901VMA", "1901VMA", "786333");
            c.setAutoCommit(false);

            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM usuario;");

            DefaultTableModel modelo = new DefaultTableModel(new Object[]{"Login", "Nome", "Cidade", "Data de nascimento"}, 0);

            while (rs.next()) {
                Object linha[] = new Object[]{rs.getString("login"), rs.getString("nome"), rs.getString("cidade"), rs.getString("birthday")};
                modelo.addRow(linha);
            }

            tbl_usuario_conhecidos.setModel(modelo);
            tbl_usuario_conhecidos.getColumnModel().getColumn(0).setPreferredWidth(300);
            tbl_usuario_conhecidos.getColumnModel().getColumn(1).setPreferredWidth(100);
            tbl_usuario_conhecidos.getColumnModel().getColumn(2).setPreferredWidth(100);
            tbl_usuario_conhecidos.getColumnModel().getColumn(3).setPreferredWidth(100);

            rs.close();
            stmt.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
            JOptionPane.showMessageDialog(null, e.getClass().getName() + ": " + e.getMessage());
        }
        JOptionPane.showMessageDialog(null, "Tabela atualizada!");

    }

    public void LoadTableUsuariosFilme() {

        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql://200.134.10.32:5432/1901VMA", "1901VMA", "786333");
            c.setAutoCommit(false);

            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM usuario;");

            DefaultTableModel modelo = new DefaultTableModel(new Object[]{"Login", "Nome", "Cidade", "Data de nascimento"}, 0);

            while (rs.next()) {
                Object linha[] = new Object[]{rs.getString("login"), rs.getString("nome"), rs.getString("cidade"), rs.getString("birthday")};
                modelo.addRow(linha);
            }

            tbl_usuario_filmes.setModel(modelo);
            tbl_usuario_filmes.getColumnModel().getColumn(0).setPreferredWidth(300);
            tbl_usuario_filmes.getColumnModel().getColumn(1).setPreferredWidth(100);
            tbl_usuario_filmes.getColumnModel().getColumn(2).setPreferredWidth(100);
            tbl_usuario_filmes.getColumnModel().getColumn(3).setPreferredWidth(100);

            rs.close();
            stmt.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
            JOptionPane.showMessageDialog(null, e.getClass().getName() + ": " + e.getMessage());
        }
        JOptionPane.showMessageDialog(null, "Tabela atualizada!");

    }
   
    public void LoadTableUsuariosArtista() {

        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql://200.134.10.32:5432/1901VMA", "1901VMA", "786333");
            c.setAutoCommit(false);

            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM usuario;");

            DefaultTableModel modelo = new DefaultTableModel(new Object[]{"Login", "Nome", "Cidade", "Data de nascimento"}, 0);

            while (rs.next()) {
                Object linha[] = new Object[]{rs.getString("login"), rs.getString("nome"), rs.getString("cidade"), rs.getString("birthday")};
                modelo.addRow(linha);
            }

            tbl_usuario_musicas.setModel(modelo);
            tbl_usuario_musicas.getColumnModel().getColumn(0).setPreferredWidth(300);
            tbl_usuario_musicas.getColumnModel().getColumn(1).setPreferredWidth(100);
            tbl_usuario_musicas.getColumnModel().getColumn(2).setPreferredWidth(100);
            tbl_usuario_musicas.getColumnModel().getColumn(3).setPreferredWidth(100);

            rs.close();
            stmt.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
            JOptionPane.showMessageDialog(null, e.getClass().getName() + ": " + e.getMessage());
        }
        JOptionPane.showMessageDialog(null, "Tabela atualizada!");

    }

    public void ListaConhecidos() {

        if (tbl_usuario_conhecidos.getSelectedRow() >= 0) {

            int row = tbl_usuario_conhecidos.getSelectedRow();
            String usu = tbl_usuario_conhecidos.getModel().getValueAt(row, 0).toString();

            Connection c = null;
            Statement stmt = null;
            try {
                Class.forName("org.postgresql.Driver");
                c = DriverManager.getConnection("jdbc:postgresql://200.134.10.32:5432/1901VMA", "1901VMA", "786333");
                c.setAutoCommit(false);

                stmt = c.createStatement();
                ResultSet rs = stmt.executeQuery("SELECT u.login, u.nome, u.cidade, u.birthday FROM Usuario u, Conhece d WHERE u.login=d.usua2 AND d.usua1='" + usu + "'");

                DefaultTableModel modelo = new DefaultTableModel(new Object[]{"Login", "Nome", "Cidade", "Data de nascimento"}, 0);

                while (rs.next()) {
                    Object linha[] = new Object[]{rs.getString("login"), rs.getString("nome"), rs.getString("cidade"), rs.getString("birthday")};
                    modelo.addRow(linha);
                }

                tbl_conhece_conhecidos.setModel(modelo);
                tbl_conhece_conhecidos.getColumnModel().getColumn(0).setPreferredWidth(300);
                tbl_conhece_conhecidos.getColumnModel().getColumn(1).setPreferredWidth(100);
                tbl_conhece_conhecidos.getColumnModel().getColumn(2).setPreferredWidth(100);
                tbl_conhece_conhecidos.getColumnModel().getColumn(3).setPreferredWidth(100);

                rs.close();
                stmt.close();
                c.close();
            } catch (Exception e) {
                System.err.println(e.getClass().getName() + ": " + e.getMessage());
                System.exit(0);
                JOptionPane.showMessageDialog(null, e.getClass().getName() + ": " + e.getMessage());
            }
            JOptionPane.showMessageDialog(null, "Tabela de conhecidos atualizada!");

        } else {
            JOptionPane.showMessageDialog(null, "Selecione um usuário.");
        }

    }

    public void ListaFilme() {

        if (tbl_usuario_filmes.getSelectedRow() >= 0) {

            int row = tbl_usuario_filmes.getSelectedRow();
            String usu = tbl_usuario_filmes.getModel().getValueAt(row, 0).toString();

            Connection c = null;
            Statement stmt = null;
            try {
                Class.forName("org.postgresql.Driver");
                c = DriverManager.getConnection("jdbc:postgresql://200.134.10.32:5432/1901VMA", "1901VMA", "786333");
                c.setAutoCommit(false);

                stmt = c.createStatement();
                ResultSet rs = stmt.executeQuery("SELECT filme, nota FROM Curte2 WHERE usuario='" + usu + "'");

                DefaultTableModel modelo = new DefaultTableModel(new Object[]{"Filme", "Nota"}, 0);

                while (rs.next()) {
                    Object linha[] = new Object[]{rs.getString("filme"), rs.getString("nota")};
                    modelo.addRow(linha);
                }

                tbl_gosta_filmes.setModel(modelo);
                tbl_gosta_filmes.getColumnModel().getColumn(0).setPreferredWidth(300);
                tbl_gosta_filmes.getColumnModel().getColumn(1).setPreferredWidth(100);

                rs.close();
                stmt.close();
                c.close();
            } catch (Exception e) {
                System.err.println(e.getClass().getName() + ": " + e.getMessage());
                System.exit(0);
                JOptionPane.showMessageDialog(null, e.getClass().getName() + ": " + e.getMessage());
            }
            JOptionPane.showMessageDialog(null, "Tabela de filmes atualizada!");

        } else {
            JOptionPane.showMessageDialog(null, "Selecione um usuário.");
        }

    }
   
    public void ListaArtista() {

        if (tbl_usuario_musicas.getSelectedRow() >= 0) {

            int row = tbl_usuario_musicas.getSelectedRow();
            String usu = tbl_usuario_musicas.getModel().getValueAt(row, 0).toString();

            Connection c = null;
            Statement stmt = null;
            try {
                Class.forName("org.postgresql.Driver");
                c = DriverManager.getConnection("jdbc:postgresql://200.134.10.32:5432/1901VMA", "1901VMA", "786333");
                c.setAutoCommit(false);

                stmt = c.createStatement();
                ResultSet rs = stmt.executeQuery("SELECT artistas, nota FROM Curte WHERE usuario='" + usu + "'");

                DefaultTableModel modelo = new DefaultTableModel(new Object[]{"Artista", "Nota"}, 0);

                while (rs.next()) {
                    Object linha[] = new Object[]{rs.getString("artistas"), rs.getString("nota")};
                    modelo.addRow(linha);
                }

                tbl_gosta_musicas.setModel(modelo);
                tbl_gosta_musicas.getColumnModel().getColumn(0).setPreferredWidth(300);
                tbl_gosta_musicas.getColumnModel().getColumn(1).setPreferredWidth(100);

                rs.close();
                stmt.close();
                c.close();
            } catch (Exception e) {
                System.err.println(e.getClass().getName() + ": " + e.getMessage());
                System.exit(0);
                JOptionPane.showMessageDialog(null, e.getClass().getName() + ": " + e.getMessage());
            }
            JOptionPane.showMessageDialog(null, "Tabela de artistas atualizada!");

        } else {
            JOptionPane.showMessageDialog(null, "Selecione um usuário.");
        }

    }

    public void CadUsuario(String login, String nome, String cidade, String birthday) {

        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql://200.134.10.32:5432/1901VMA", "1901VMA", "786333");
            stmt = c.createStatement();
            String AddUsuario = "INSERT INTO usuario VALUES('" + login + "','" + nome + "','" + cidade + "','" + birthday + "')";
            stmt.executeUpdate(AddUsuario);
            stmt.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
            JOptionPane.showMessageDialog(null, e.getClass().getName() + ": " + e.getMessage());
        }
        JOptionPane.showMessageDialog(null, "Operation done successfully");

    }

    public void DelUsuario(String login) {

        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql://200.134.10.32:5432/1901VMA", "1901VMA", "786333");
            stmt = c.createStatement();
            String DelUsuario = "DELETE FROM conhece WHERE usua1='" + login + "' or usua2='" + login + "';"
                    + "DELETE FROM curte WHERE usuario='" + login + "';"
                    + "DELETE FROM curte2 WHERE usuario='" + login + "';"
                    + "DELETE FROM bloqueio WHERE usuario='" + login + "' or usuario_block='" + login + "';"
                    + "DELETE FROM usuario WHERE login='" + login + "';";

            stmt.executeUpdate(DelUsuario);
            stmt.close();

            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
            JOptionPane.showMessageDialog(null, e.getClass().getName() + ": " + e.getMessage());
        }
        JOptionPane.showMessageDialog(null, "Operation done successfully");

    }

    public void EditUsuario(String login, String nome, String cidade, String birthday) {
        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql://200.134.10.32:5432/1901VMA", "1901VMA", "786333");
            stmt = c.createStatement();
            String EditUsuario = "UPDATE usuario SET nome='" + nome
                    + "',cidade='" + cidade
                    + "',birthday='" + birthday + "'"
                    + "WHERE login='" + login + "';";

            stmt.executeUpdate(EditUsuario);
            stmt.close();

            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
            JOptionPane.showMessageDialog(null, e.getClass().getName() + ": " + e.getMessage());
        }
        JOptionPane.showMessageDialog(null, "Operation done successfully");

    }

    public void ConheceUsuario(String usuario, String amigo) {

        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql://200.134.10.32:5432/1901VMA", "1901VMA", "786333");
            stmt = c.createStatement();
            String AddAmigo = "INSERT INTO conhece VALUES('" + usuario + "','" + amigo + "');";
            stmt.executeUpdate(AddAmigo);
            stmt.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
            JOptionPane.showMessageDialog(null, e.getClass().getName() + ": " + e.getMessage());
        }
        JOptionPane.showMessageDialog(null, "Operation done successfully");
    }

    public void CurteFilme(String usuario, String filme, int nota) {

        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql://200.134.10.32:5432/1901VMA", "1901VMA", "786333");
            stmt = c.createStatement();

            //Add o filme caso ainda nao esteja no banco de dados
            try {
                String AddFilme = "INSERT INTO filme VALUES('" + filme + "','1901-12-12')";
                stmt.executeUpdate(AddFilme);
            } catch (Exception e) {
                System.err.println(e.getClass().getName() + ": " + e.getMessage());
            }
            //Add o filme aos gostos
            try {
                String AddCurteFilme = "INSERT INTO curte2 VALUES('" + nota + "','" + usuario + "','" + filme + "')";
                stmt.executeUpdate(AddCurteFilme);

            } catch (Exception e) {
                System.err.println(e.getClass().getName() + ": " + e.getMessage());
                System.exit(0);
                JOptionPane.showMessageDialog(null, e.getClass().getName() + ": " + e.getMessage());
            }
            stmt.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
            JOptionPane.showMessageDialog(null, e.getClass().getName() + ": " + e.getMessage());
        }

        JOptionPane.showMessageDialog(null, "Operation done successfully");

    }

    public void ExcluiFilme() {

        if (tbl_gosta_filmes.getSelectedRow() >= 0) {
            
            int row = tbl_gosta_filmes.getSelectedRow();
            String filme = tbl_gosta_filmes.getModel().getValueAt(row, 0).toString();

            Connection c = null;
            Statement stmt = null;
            try {
                Class.forName("org.postgresql.Driver");
                c = DriverManager.getConnection("jdbc:postgresql://200.134.10.32:5432/1901VMA", "1901VMA", "786333");
                stmt = c.createStatement();
                String DelFilme = "DELETE FROM curte2 WHERE filme='"+filme+"'";

                stmt.executeUpdate(DelFilme);
                stmt.close();

                c.close();
            } catch (Exception e) {
                System.err.println(e.getClass().getName() + ": " + e.getMessage());
                System.exit(0);
                JOptionPane.showMessageDialog(null, e.getClass().getName() + ": " + e.getMessage());
            }
            JOptionPane.showMessageDialog(null, "Operation done successfully");
        } else {
            JOptionPane.showMessageDialog(null, "Selecione um filme.");
        }

    }
    
    public void ExcluiArtista() {

        if (tbl_gosta_musicas.getSelectedRow() >= 0) {
            
            int row = tbl_gosta_musicas.getSelectedRow();
            String artista = tbl_gosta_musicas.getModel().getValueAt(row, 0).toString();

            Connection c = null;
            Statement stmt = null;
            try {
                Class.forName("org.postgresql.Driver");
                c = DriverManager.getConnection("jdbc:postgresql://200.134.10.32:5432/1901VMA", "1901VMA", "786333");
                stmt = c.createStatement();
                String DelArtista = "DELETE FROM curte WHERE artistas='"+artista+"'";

                stmt.executeUpdate(DelArtista);
                stmt.close();

                c.close();
            } catch (Exception e) {
                System.err.println(e.getClass().getName() + ": " + e.getMessage());
                System.exit(0);
                JOptionPane.showMessageDialog(null, e.getClass().getName() + ": " + e.getMessage());
            }
            JOptionPane.showMessageDialog(null, "Operation done successfully");
        } else {
            JOptionPane.showMessageDialog(null, "Selecione um artista.");
        }

    }
    public void ExcluiAmigo() {

        if (tbl_conhece_conhecidos.getSelectedRow() >= 0) {
            
            int row = tbl_conhece_conhecidos.getSelectedRow();
            int row2 = tbl_usuario_conhecidos.getSelectedRow();
            String amigo = tbl_conhece_conhecidos.getModel().getValueAt(row, 0).toString();
            String usuario = tbl_usuario_conhecidos.getModel().getValueAt(row2, 0).toString();

            Connection c = null;
            Statement stmt = null;
            try {
                Class.forName("org.postgresql.Driver");
                c = DriverManager.getConnection("jdbc:postgresql://200.134.10.32:5432/1901VMA", "1901VMA", "786333");
                stmt = c.createStatement();
                String DelAmigo = "DELETE FROM conhece WHERE usua1='"+usuario+"' AND usua2='"+amigo+"'";

                stmt.executeUpdate(DelAmigo);
                stmt.close();

                c.close();
            } catch (Exception e) {
                System.err.println(e.getClass().getName() + ": " + e.getMessage());
                System.exit(0);
                JOptionPane.showMessageDialog(null, e.getClass().getName() + ": " + e.getMessage());
            }
            JOptionPane.showMessageDialog(null, "Operation done successfully");
        } else {
            JOptionPane.showMessageDialog(null, "Selecione um Conhecido.");
        }

    }

    public void CurteMusica(String usuario, String artista, int nota) {

        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql://200.134.10.32:5432/1901VMA", "1901VMA", "786333");
            stmt = c.createStatement();

            //Add o artista caso ainda nao esteja no banco de dados
            try {
                String AddArtista = "INSERT INTO artistas VALUES('PANGEIA','" + artista + "','SURPRESA');";
                stmt.executeUpdate(AddArtista);
            } catch (Exception e) {
                System.err.println(e.getClass().getName() + ": " + e.getMessage());
            }
            //Add o filme aos gostos
            try {
                String AddCurteArtista = "INSERT INTO curte VALUES('" + nota + "','" + usuario + "','" + artista + "');";
                stmt.executeUpdate(AddCurteArtista);

            } catch (Exception e) {
                System.err.println(e.getClass().getName() + ": " + e.getMessage());
                System.exit(0);
                JOptionPane.showMessageDialog(null, e.getClass().getName() + ": " + e.getMessage());
            }
            stmt.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
            JOptionPane.showMessageDialog(null, e.getClass().getName() + ": " + e.getMessage());
        }

        JOptionPane.showMessageDialog(null, "Operation done successfully");

    }

    /**
     * Creates new form Principal
     */
    public Principal() {
        initComponents();
        setLocationRelativeTo(null);
        EstadoUsuario();
        EstadoFilme();
        EstadoArtista();
        EstadoAmigo();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblUsuarios = new javax.swing.JTable();
        btnConsultar = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        txtb_login_usuarios = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtb_nome_usuarios = new javax.swing.JTextField();
        txtb_cidade_usuarios = new javax.swing.JTextField();
        btn_cancelar_usuarios = new javax.swing.JButton();
        btn_salvar_usuarios = new javax.swing.JButton();
        txtb_data_usuarios = new javax.swing.JTextField();
        btn_novo_usuarios = new javax.swing.JButton();
        btn_excluir_usuarios = new javax.swing.JButton();
        btn_editar_usuarios = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tbl_gosta_filmes = new javax.swing.JTable();
        jScrollPane5 = new javax.swing.JScrollPane();
        tbl_usuario_filmes = new javax.swing.JTable();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        btn_listarusu_filmes = new javax.swing.JButton();
        btn_novo_filme = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        txt_nome_filme = new javax.swing.JTextField();
        btn_cancelar_filme = new javax.swing.JButton();
        jLabel12 = new javax.swing.JLabel();
        txtb_nota_filme = new javax.swing.JTextField();
        btn_add_filme = new javax.swing.JButton();
        btn_excluir_filme = new javax.swing.JButton();
        btn_listarfilm_filmes = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        tbl_gosta_musicas = new javax.swing.JTable();
        jScrollPane7 = new javax.swing.JScrollPane();
        tbl_usuario_musicas = new javax.swing.JTable();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        ListUsuMus = new javax.swing.JButton();
        btn_listar_artista = new javax.swing.JButton();
        btn_novo_artista = new javax.swing.JButton();
        btn_excluir_artista = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        txt_nome_artista = new javax.swing.JTextField();
        btn_cancelar_artista = new javax.swing.JButton();
        jLabel14 = new javax.swing.JLabel();
        txtb_nota_artista = new javax.swing.JTextField();
        btn_add_artista = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbl_usuario_conhecidos = new javax.swing.JTable();
        jScrollPane4 = new javax.swing.JScrollPane();
        tbl_conhece_conhecidos = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        btn_listarusu_conhecidos = new javax.swing.JButton();
        btn_listarcon_conhecidos = new javax.swing.JButton();
        btn_novo_conhecido = new javax.swing.JButton();
        jPanel8 = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        txt_nome_amigo = new javax.swing.JTextField();
        btn_cancelar_amigo = new javax.swing.JButton();
        btn_add_amigo = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();

        jMenu1.setText("jMenu1");

        jMenu2.setText("jMenu2");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        tblUsuarios.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Login", "Nome", "Cidade", "Data de nascimento"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tblUsuarios);
        if (tblUsuarios.getColumnModel().getColumnCount() > 0) {
            tblUsuarios.getColumnModel().getColumn(0).setPreferredWidth(300);
            tblUsuarios.getColumnModel().getColumn(1).setPreferredWidth(100);
            tblUsuarios.getColumnModel().getColumn(2).setPreferredWidth(100);
            tblUsuarios.getColumnModel().getColumn(3).setPreferredWidth(100);
        }

        btnConsultar.setText("Consultar");
        btnConsultar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConsultarActionPerformed(evt);
            }
        });

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Dados"));

        txtb_login_usuarios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtb_login_usuariosActionPerformed(evt);
            }
        });

        jLabel7.setText("Login:");

        jLabel8.setText("Nome:");

        jLabel9.setText("Cidade:");

        jLabel10.setText("Data de nascimento:");

        txtb_nome_usuarios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtb_nome_usuariosActionPerformed(evt);
            }
        });

        txtb_cidade_usuarios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtb_cidade_usuariosActionPerformed(evt);
            }
        });

        btn_cancelar_usuarios.setText("Cancelar");
        btn_cancelar_usuarios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelar_usuariosActionPerformed(evt);
            }
        });

        btn_salvar_usuarios.setText("Salvar");
        btn_salvar_usuarios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_salvar_usuariosActionPerformed(evt);
            }
        });

        txtb_data_usuarios.setText("AAAA-MM-DD");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtb_data_usuarios, javax.swing.GroupLayout.DEFAULT_SIZE, 335, Short.MAX_VALUE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8)
                            .addComponent(jLabel9)
                            .addComponent(jLabel7))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtb_login_usuarios)
                            .addComponent(txtb_nome_usuarios)
                            .addComponent(txtb_cidade_usuarios))))
                .addGap(109, 109, 109)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btn_cancelar_usuarios, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE)
                    .addComponent(btn_salvar_usuarios, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE))
                .addGap(93, 93, 93))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtb_login_usuarios, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(txtb_nome_usuarios, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel9)
                            .addComponent(txtb_cidade_usuarios, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(btn_salvar_usuarios)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btn_cancelar_usuarios)))
                .addGap(12, 12, 12)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(txtb_data_usuarios, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(35, Short.MAX_VALUE))
        );

        btn_novo_usuarios.setText("Novo");
        btn_novo_usuarios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_novo_usuariosActionPerformed(evt);
            }
        });

        btn_excluir_usuarios.setText("Excluir");
        btn_excluir_usuarios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_excluir_usuariosActionPerformed(evt);
            }
        });

        btn_editar_usuarios.setText("Editar");
        btn_editar_usuarios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_editar_usuariosActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btn_excluir_usuarios, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(btnConsultar, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btn_novo_usuarios, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btn_editar_usuarios, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 18, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 351, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(47, 47, 47)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnConsultar)
                    .addComponent(btn_novo_usuarios)
                    .addComponent(btn_editar_usuarios))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 212, Short.MAX_VALUE)
                        .addComponent(btn_excluir_usuarios)
                        .addGap(200, 200, 200))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        jTabbedPane1.addTab("Usuários", jPanel1);

        tbl_gosta_filmes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nome", "Nota"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane3.setViewportView(tbl_gosta_filmes);
        if (tbl_gosta_filmes.getColumnModel().getColumnCount() > 0) {
            tbl_gosta_filmes.getColumnModel().getColumn(0).setPreferredWidth(300);
            tbl_gosta_filmes.getColumnModel().getColumn(1).setPreferredWidth(100);
        }

        tbl_usuario_filmes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Login", "Nome", "Cidade", "Data de nascimento"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane5.setViewportView(tbl_usuario_filmes);
        if (tbl_usuario_filmes.getColumnModel().getColumnCount() > 0) {
            tbl_usuario_filmes.getColumnModel().getColumn(0).setPreferredWidth(300);
            tbl_usuario_filmes.getColumnModel().getColumn(1).setPreferredWidth(100);
            tbl_usuario_filmes.getColumnModel().getColumn(2).setPreferredWidth(100);
            tbl_usuario_filmes.getColumnModel().getColumn(2).setHeaderValue("Cidade");
            tbl_usuario_filmes.getColumnModel().getColumn(3).setPreferredWidth(100);
            tbl_usuario_filmes.getColumnModel().getColumn(3).setHeaderValue("Data de nascimento");
        }

        jLabel3.setText("Usuário:");

        jLabel4.setText("Gosta:");

        btn_listarusu_filmes.setText("Listar usuários");
        btn_listarusu_filmes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_listarusu_filmesActionPerformed(evt);
            }
        });

        btn_novo_filme.setText("Novo");
        btn_novo_filme.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_novo_filmeActionPerformed(evt);
            }
        });

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Dados"));

        jLabel11.setText("Nome:");

        txt_nome_filme.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_nome_filmeActionPerformed(evt);
            }
        });

        btn_cancelar_filme.setText("Cancelar");
        btn_cancelar_filme.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelar_filmeActionPerformed(evt);
            }
        });

        jLabel12.setText("Nota:");

        txtb_nota_filme.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtb_nota_filmeActionPerformed(evt);
            }
        });

        btn_add_filme.setText("Adicionar");
        btn_add_filme.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_add_filmeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_nome_filme, javax.swing.GroupLayout.PREFERRED_SIZE, 455, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addComponent(btn_add_filme, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btn_cancelar_filme, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(txtb_nota_filme, javax.swing.GroupLayout.PREFERRED_SIZE, 455, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(txt_nome_filme, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(txtb_nota_filme, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_add_filme)
                    .addComponent(btn_cancelar_filme))
                .addGap(0, 252, Short.MAX_VALUE))
        );

        btn_excluir_filme.setText("Excluir");
        btn_excluir_filme.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_excluir_filmeActionPerformed(evt);
            }
        });

        btn_listarfilm_filmes.setText("Listar filmes");
        btn_listarfilm_filmes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_listarfilm_filmesActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 984, Short.MAX_VALUE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)
                            .addComponent(btn_listarusu_filmes, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(btn_listarfilm_filmes, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btn_novo_filme, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btn_excluir_filme, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 984, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addGap(196, 196, 196)
                .addComponent(btn_listarusu_filmes)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_novo_filme)
                    .addComponent(btn_excluir_filme)
                    .addComponent(btn_listarfilm_filmes))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel3Layout.createSequentialGroup()
                    .addGap(37, 37, 37)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(654, Short.MAX_VALUE)))
        );

        jTabbedPane1.addTab("Filmes", jPanel3);

        tbl_gosta_musicas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nome", "Nota"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane6.setViewportView(tbl_gosta_musicas);
        if (tbl_gosta_musicas.getColumnModel().getColumnCount() > 0) {
            tbl_gosta_musicas.getColumnModel().getColumn(0).setPreferredWidth(100);
            tbl_gosta_musicas.getColumnModel().getColumn(1).setPreferredWidth(100);
        }

        tbl_usuario_musicas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Login", "Nome", "Cidade", "Data de nascimento"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane7.setViewportView(tbl_usuario_musicas);
        if (tbl_usuario_musicas.getColumnModel().getColumnCount() > 0) {
            tbl_usuario_musicas.getColumnModel().getColumn(0).setPreferredWidth(300);
            tbl_usuario_musicas.getColumnModel().getColumn(1).setPreferredWidth(100);
            tbl_usuario_musicas.getColumnModel().getColumn(2).setPreferredWidth(100);
            tbl_usuario_musicas.getColumnModel().getColumn(2).setHeaderValue("Cidade");
            tbl_usuario_musicas.getColumnModel().getColumn(3).setPreferredWidth(100);
            tbl_usuario_musicas.getColumnModel().getColumn(3).setHeaderValue("Data de nascimento");
        }

        jLabel5.setText("Usuário:");

        jLabel6.setText("Gosta:");

        ListUsuMus.setText("Listar Usuários");
        ListUsuMus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ListUsuMusActionPerformed(evt);
            }
        });

        btn_listar_artista.setText("Listar Artistas");
        btn_listar_artista.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_listar_artistaActionPerformed(evt);
            }
        });

        btn_novo_artista.setText("Novo");
        btn_novo_artista.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_novo_artistaActionPerformed(evt);
            }
        });

        btn_excluir_artista.setText("Excluir");
        btn_excluir_artista.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_excluir_artistaActionPerformed(evt);
            }
        });

        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Dados"));

        jLabel13.setText("Nome:");

        txt_nome_artista.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_nome_artistaActionPerformed(evt);
            }
        });

        btn_cancelar_artista.setText("Cancelar");
        btn_cancelar_artista.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelar_artistaActionPerformed(evt);
            }
        });

        jLabel14.setText("Nota:");

        txtb_nota_artista.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtb_nota_artistaActionPerformed(evt);
            }
        });

        btn_add_artista.setText("Adicionar");
        btn_add_artista.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_add_artistaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(jLabel13)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_nome_artista, javax.swing.GroupLayout.PREFERRED_SIZE, 455, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jLabel14)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addComponent(btn_add_artista, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btn_cancelar_artista, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(txtb_nota_artista, javax.swing.GroupLayout.PREFERRED_SIZE, 455, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(txt_nome_artista, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(txtb_nota_artista, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_add_artista)
                    .addComponent(btn_cancelar_artista))
                .addGap(0, 40, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 984, Short.MAX_VALUE)
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 984, Short.MAX_VALUE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6)
                            .addComponent(ListUsuMus)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(btn_listar_artista)
                                .addGap(30, 30, 30)
                                .addComponent(btn_novo_artista)
                                .addGap(32, 32, 32)
                                .addComponent(btn_excluir_artista)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jPanel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ListUsuMus)
                .addGap(14, 14, 14)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_listar_artista)
                    .addComponent(btn_novo_artista)
                    .addComponent(btn_excluir_artista))
                .addGap(30, 30, 30)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(187, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Músicas", jPanel4);

        tbl_usuario_conhecidos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Login", "Nome", "Cidade", "Data de nascimento"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(tbl_usuario_conhecidos);
        if (tbl_usuario_conhecidos.getColumnModel().getColumnCount() > 0) {
            tbl_usuario_conhecidos.getColumnModel().getColumn(0).setPreferredWidth(300);
            tbl_usuario_conhecidos.getColumnModel().getColumn(1).setPreferredWidth(100);
            tbl_usuario_conhecidos.getColumnModel().getColumn(2).setPreferredWidth(100);
            tbl_usuario_conhecidos.getColumnModel().getColumn(2).setHeaderValue("Cidade");
            tbl_usuario_conhecidos.getColumnModel().getColumn(3).setPreferredWidth(100);
            tbl_usuario_conhecidos.getColumnModel().getColumn(3).setHeaderValue("Data de nascimento");
        }

        tbl_conhece_conhecidos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Login", "Nome", "Cidade", "Data de nascimento"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane4.setViewportView(tbl_conhece_conhecidos);
        if (tbl_conhece_conhecidos.getColumnModel().getColumnCount() > 0) {
            tbl_conhece_conhecidos.getColumnModel().getColumn(0).setPreferredWidth(300);
            tbl_conhece_conhecidos.getColumnModel().getColumn(1).setPreferredWidth(100);
            tbl_conhece_conhecidos.getColumnModel().getColumn(2).setPreferredWidth(100);
            tbl_conhece_conhecidos.getColumnModel().getColumn(2).setHeaderValue("Cidade");
            tbl_conhece_conhecidos.getColumnModel().getColumn(3).setPreferredWidth(100);
            tbl_conhece_conhecidos.getColumnModel().getColumn(3).setHeaderValue("Data de nascimento");
        }

        jLabel1.setText("Usuário:");

        jLabel2.setText("Conhece:");

        btn_listarusu_conhecidos.setText("Listar usuários");
        btn_listarusu_conhecidos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_listarusu_conhecidosActionPerformed(evt);
            }
        });

        btn_listarcon_conhecidos.setText("Listar conhecidos");
        btn_listarcon_conhecidos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_listarcon_conhecidosActionPerformed(evt);
            }
        });

        btn_novo_conhecido.setText("Novo");
        btn_novo_conhecido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_novo_conhecidoActionPerformed(evt);
            }
        });

        jPanel8.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Dados"));

        jLabel15.setText("Login do amigo:");

        txt_nome_amigo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_nome_amigoActionPerformed(evt);
            }
        });

        btn_cancelar_amigo.setText("Cancelar");
        btn_cancelar_amigo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelar_amigoActionPerformed(evt);
            }
        });

        btn_add_amigo.setText("Adicionar");
        btn_add_amigo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_add_amigoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel15)
                .addGap(18, 18, 18)
                .addComponent(txt_nome_amigo, javax.swing.GroupLayout.PREFERRED_SIZE, 455, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(416, Short.MAX_VALUE))
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addComponent(btn_add_amigo, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(44, 44, 44)
                .addComponent(btn_cancelar_amigo, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGap(51, 51, 51)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(txt_nome_amigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(38, 38, 38)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_add_amigo)
                    .addComponent(btn_cancelar_amigo))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jButton1.setText("Excluir");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane4)
                    .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel1)
                            .addComponent(btn_listarusu_conhecidos, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(btn_listarcon_conhecidos, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btn_novo_conhecido, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButton1)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btn_listarusu_conhecidos)
                .addGap(27, 27, 27)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_listarcon_conhecidos)
                    .addComponent(btn_novo_conhecido)
                    .addComponent(jButton1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(214, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Conhecidos", jPanel2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnConsultarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConsultarActionPerformed
        // TODO add your handling code here:

        LoadTableUsuarios();
    }//GEN-LAST:event_btnConsultarActionPerformed

    private void btn_salvar_usuariosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_salvar_usuariosActionPerformed
        // TODO add your handling code here:
        if (estadoUsuario == "Novo") {
            CadUsuario(txtb_login_usuarios.getText(), txtb_nome_usuarios.getText(), txtb_cidade_usuarios.getText(), txtb_data_usuarios.getText());
        } else if (estadoUsuario == "Editar") {
            EditUsuario(txtb_login_usuarios.getText(), txtb_nome_usuarios.getText(), txtb_cidade_usuarios.getText(), txtb_data_usuarios.getText());
        }

        txtb_login_usuarios.setText("");
        txtb_nome_usuarios.setText("");
        txtb_cidade_usuarios.setText("");
        txtb_data_usuarios.setText("AAAA-MM-DD");

        estadoUsuario = "Cancelar";
        EstadoUsuario();

        LoadTableUsuarios();
    }//GEN-LAST:event_btn_salvar_usuariosActionPerformed

    private void btn_cancelar_usuariosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelar_usuariosActionPerformed
        // TODO add your handling code here:

        txtb_login_usuarios.setText("");
        txtb_nome_usuarios.setText("");
        txtb_cidade_usuarios.setText("");
        txtb_data_usuarios.setText("AAAA-MM-DD");

        estadoUsuario = "Cancelar";
        EstadoUsuario();
    }//GEN-LAST:event_btn_cancelar_usuariosActionPerformed

    private void txtb_cidade_usuariosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtb_cidade_usuariosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtb_cidade_usuariosActionPerformed

    private void txtb_nome_usuariosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtb_nome_usuariosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtb_nome_usuariosActionPerformed

    private void txtb_login_usuariosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtb_login_usuariosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtb_login_usuariosActionPerformed

    private void btn_novo_usuariosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_novo_usuariosActionPerformed
        // TODO add your handling code here:

        estadoUsuario = "Novo";
        EstadoUsuario();
    }//GEN-LAST:event_btn_novo_usuariosActionPerformed

    private void btn_excluir_usuariosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_excluir_usuariosActionPerformed
        // TODO add your handling code here:

        if (tblUsuarios.getSelectedRow() >= 0) {

            int row = tblUsuarios.getSelectedRow();
            String usu = tblUsuarios.getModel().getValueAt(row, 0).toString();

            DelUsuario(usu);

        } else {
            JOptionPane.showMessageDialog(null, "Selecione um usuário.");
        }

    }//GEN-LAST:event_btn_excluir_usuariosActionPerformed

    private void btn_editar_usuariosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_editar_usuariosActionPerformed
        // TODO add your handling code here:

        if (tblUsuarios.getSelectedRow() >= 0) {

            estadoUsuario = "Editar";
            EstadoUsuario();

            int row = tblUsuarios.getSelectedRow();
            String login = tblUsuarios.getModel().getValueAt(row, 0).toString();
            String nome = tblUsuarios.getModel().getValueAt(row, 1).toString();
            String cidade = tblUsuarios.getModel().getValueAt(row, 2).toString();
            String data = tblUsuarios.getModel().getValueAt(row, 3).toString();

            txtb_login_usuarios.setText(login);
            txtb_nome_usuarios.setText(nome);
            txtb_cidade_usuarios.setText(cidade);
            txtb_data_usuarios.setText(data);

        } else {
            JOptionPane.showMessageDialog(null, "Selecione um usuário.");
        }

    }//GEN-LAST:event_btn_editar_usuariosActionPerformed

    private void btn_listarusu_conhecidosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_listarusu_conhecidosActionPerformed
        // TODO add your handling code here:

        LoadTableUsuariosConhecidos();
    }//GEN-LAST:event_btn_listarusu_conhecidosActionPerformed

    private void btn_listarcon_conhecidosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_listarcon_conhecidosActionPerformed
        // TODO add your handling code here:

        ListaConhecidos();
    }//GEN-LAST:event_btn_listarcon_conhecidosActionPerformed

    private void txt_nome_filmeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_nome_filmeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_nome_filmeActionPerformed

    private void btn_novo_filmeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_novo_filmeActionPerformed
        // TODO add your handling code here:

        estadoFilme = "Novo";
        EstadoFilme();


    }//GEN-LAST:event_btn_novo_filmeActionPerformed

    private void btn_cancelar_filmeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelar_filmeActionPerformed
        // TODO add your handling code here:

        txt_nome_filme.setText("");
        txtb_nota_filme.setText("");

        estadoFilme = "Cancelar";
        EstadoFilme();
    }//GEN-LAST:event_btn_cancelar_filmeActionPerformed

    private void btn_excluir_filmeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_excluir_filmeActionPerformed
        // TODO add your handling code here:
        
        ExcluiFilme();
    }//GEN-LAST:event_btn_excluir_filmeActionPerformed

    private void btn_listarusu_filmesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_listarusu_filmesActionPerformed
        // TODO add your handling code here:

        LoadTableUsuariosFilme();
    }//GEN-LAST:event_btn_listarusu_filmesActionPerformed

    private void btn_listarfilm_filmesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_listarfilm_filmesActionPerformed
        // TODO add your handling code here:

        ListaFilme();
    }//GEN-LAST:event_btn_listarfilm_filmesActionPerformed

    private void txtb_nota_filmeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtb_nota_filmeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtb_nota_filmeActionPerformed

    private void btn_add_filmeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_add_filmeActionPerformed
        // TODO add your handling code here:

        if (tbl_usuario_filmes.getSelectedRow() >= 0) {

            int row = tbl_usuario_filmes.getSelectedRow();
            String usu = tbl_usuario_filmes.getModel().getValueAt(row, 0).toString();

            CurteFilme(usu, txt_nome_filme.getText(), Integer.parseInt(txtb_nota_filme.getText()));

            txt_nome_filme.setText("");
            txtb_nota_filme.setText("");

            estadoFilme = "Cancelar";
            EstadoFilme();
        }
    }//GEN-LAST:event_btn_add_filmeActionPerformed

    private void ListUsuMusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ListUsuMusActionPerformed
        LoadTableUsuariosArtista();
        // TODO add your handling code here:
    }//GEN-LAST:event_ListUsuMusActionPerformed

    private void txt_nome_artistaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_nome_artistaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_nome_artistaActionPerformed

    private void btn_cancelar_artistaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelar_artistaActionPerformed

        txt_nome_artista.setText("");
        txtb_nota_artista.setText("");

        estadoArtista = "Cancelar";
        EstadoArtista();        // TODO add your handling code here:
    }//GEN-LAST:event_btn_cancelar_artistaActionPerformed

    private void txtb_nota_artistaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtb_nota_artistaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtb_nota_artistaActionPerformed

    private void btn_add_artistaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_add_artistaActionPerformed
        if (tbl_usuario_musicas.getSelectedRow() >= 0) {

            int row = tbl_usuario_musicas.getSelectedRow();
            String usu = tbl_usuario_musicas.getModel().getValueAt(row, 0).toString();

            CurteMusica(usu, txt_nome_artista.getText(), Integer.parseInt(txtb_nota_artista.getText()));

            txt_nome_artista.setText("");
            txtb_nota_artista.setText("");

            estadoArtista = "Cancelar";
            EstadoArtista();
        }        // TODO add your handling code here:
    }//GEN-LAST:event_btn_add_artistaActionPerformed

    private void btn_listar_artistaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_listar_artistaActionPerformed

           ListaArtista();
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_listar_artistaActionPerformed

    private void btn_novo_artistaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_novo_artistaActionPerformed

        estadoArtista = "Novo";
        EstadoArtista();
// TODO add your handling code here:
    }//GEN-LAST:event_btn_novo_artistaActionPerformed

    private void btn_excluir_artistaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_excluir_artistaActionPerformed
   
        ExcluiArtista();
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_excluir_artistaActionPerformed

    private void btn_novo_conhecidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_novo_conhecidoActionPerformed
        
        estadoAmigo = "Novo";
        EstadoAmigo();
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_novo_conhecidoActionPerformed

    private void txt_nome_amigoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_nome_amigoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_nome_amigoActionPerformed

    private void btn_cancelar_amigoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelar_amigoActionPerformed
        txt_nome_amigo.setText("");

        estadoAmigo = "Cancelar";
        EstadoAmigo();         // TODO add your handling code here:
    }//GEN-LAST:event_btn_cancelar_amigoActionPerformed

    private void btn_add_amigoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_add_amigoActionPerformed
            if (tbl_usuario_conhecidos.getSelectedRow() >= 0) {

            int row = tbl_usuario_conhecidos.getSelectedRow();
            String usu = tbl_usuario_conhecidos.getModel().getValueAt(row, 0).toString();

            ConheceUsuario(usu,txt_nome_amigo.getText());

            txt_nome_amigo.setText("");


            estadoAmigo = "Cancelar";
            EstadoAmigo();
        } 
    }//GEN-LAST:event_btn_add_amigoActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        ExcluiAmigo();
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Principal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ListUsuMus;
    private javax.swing.JButton btnConsultar;
    private javax.swing.JButton btn_add_amigo;
    private javax.swing.JButton btn_add_artista;
    private javax.swing.JButton btn_add_filme;
    private javax.swing.JButton btn_cancelar_amigo;
    private javax.swing.JButton btn_cancelar_artista;
    private javax.swing.JButton btn_cancelar_filme;
    private javax.swing.JButton btn_cancelar_usuarios;
    private javax.swing.JButton btn_editar_usuarios;
    private javax.swing.JButton btn_excluir_artista;
    private javax.swing.JButton btn_excluir_filme;
    private javax.swing.JButton btn_excluir_usuarios;
    private javax.swing.JButton btn_listar_artista;
    private javax.swing.JButton btn_listarcon_conhecidos;
    private javax.swing.JButton btn_listarfilm_filmes;
    private javax.swing.JButton btn_listarusu_conhecidos;
    private javax.swing.JButton btn_listarusu_filmes;
    private javax.swing.JButton btn_novo_artista;
    private javax.swing.JButton btn_novo_conhecido;
    private javax.swing.JButton btn_novo_filme;
    private javax.swing.JButton btn_novo_usuarios;
    private javax.swing.JButton btn_salvar_usuarios;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable tblUsuarios;
    private javax.swing.JTable tbl_conhece_conhecidos;
    private javax.swing.JTable tbl_gosta_filmes;
    private javax.swing.JTable tbl_gosta_musicas;
    private javax.swing.JTable tbl_usuario_conhecidos;
    private javax.swing.JTable tbl_usuario_filmes;
    private javax.swing.JTable tbl_usuario_musicas;
    private javax.swing.JTextField txt_nome_amigo;
    private javax.swing.JTextField txt_nome_artista;
    private javax.swing.JTextField txt_nome_filme;
    private javax.swing.JTextField txtb_cidade_usuarios;
    private javax.swing.JTextField txtb_data_usuarios;
    private javax.swing.JTextField txtb_login_usuarios;
    private javax.swing.JTextField txtb_nome_usuarios;
    private javax.swing.JTextField txtb_nota_artista;
    private javax.swing.JTextField txtb_nota_filme;
    // End of variables declaration//GEN-END:variables
}
